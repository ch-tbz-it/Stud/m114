# A.4 Bildcodierung
*Dauer: 4 Lektionen<br>
Autor: ARJ<br>
Letzte Änderung: 16.6.2023*

# A4.1 Wie kommt Farbe in unsere Welt?

* Elektromagnetische Wellen werden mit der Wellenlänge oder der Frequenz angegeben.
* Licht besteht aus elektromagnetischen Wellen.
* Nur ein Teil der Frequenzen des Lichts sind für das menschliche Auge sichtbar.

![Farbmodelle](/GITressourcen/Daten_codieren/Wellen.jpg)

Trifft Licht auf einen Gegenstand, so werden gewisse Frequenzen absorbiert (aufgenommen) und andere reflektiert (zurückgeworfen): Wir sehen eine bestimmte Farbe.<br>
Schwarz absorbiert alle Frequenzen. Weiss reflektiert alle Frequenzen. Rot zum Beispiel entsteht, indem die roten Anteile reflektiert und der Rest absorbiert werden.<br><br>

# A4.2. Farbmodelle
![Farbmodelle](/GITressourcen/Daten_codieren/Farbmodelle.jpg)
<br>

# A4.3. Farbcodierung RGB / CMYK
Jedes Pixel setzt sich aus drei Subpixel RGB zusammen. Das Subpixel wird normalerweise mit 8 Bit aufgelöst. Bsp.: JPG, TIF
![Farbcodierung](/GITressourcen/Daten_codieren/Farbcodierung.jpg)

Probieren sie es gleich selbst aus:
* [RGB-Farbenmixer Hexadezimal](https://www.w3schools.com/colors/colors_hexadecimal.asp)
* [RGB-Farbenmixer Dezimal](https://www.w3schools.com/colors/colors_rgb.asp)
* [CMYK-Farbenmixer Prozentangabe](https://www.w3schools.com/colors/colors_cmyk.asp)

Bestimmen sie die Farben für die folgenden RGB-Farbcodes (in HEX). Nutzen sie den RGB-Farbenmixer:

* #FF0000 entspricht der Farbe ....
* #00FF00 entspricht der Farbe ....
* #0000FF entspricht der Farbe ....
* #FFFF00 entspricht der Farbe ....
* #00FFFF entspricht der Farbe ....
* #FF00FF entspricht der Farbe ....
* #000000 entspricht der Farbe ....
* #FFFFFF entspricht der Farbe ....
* #00BC00 entspricht der Farbe ....

Bestimmen sie die Farben für die folgenden prozentualen CMYK-Angaben. Nutzen sie den CMYK-Farbenmixer:

* C:0%, M:100%, Y:100%, K:0% entspricht der Farbe ....
* C:100%, M:0%, Y:100%, K:0% entspricht der Farbe ....
* C:100%, M:100%, Y:0%, K:0% entspricht der Farbe ....
* C:0%, M:0%, Y:100%, K:0% entspricht der Farbe ....
* C:100%, M:0%, Y:0%, K:0% entspricht der Farbe ....
* C:0%, M:100%, Y:0%, K:0% entspricht der Farbe ....
* C:100%, M:100%, Y:100%, K:0% entspricht der Farbe ....
* C:0%, M:0%, Y:0%, K:100% entspricht der Farbe ....
* C:0%, M:0%, Y:0%, K:0% entspricht der Farbe ....
* C:0%, M:46%, Y:38%, K:22% entspricht der Farbe ....
<br>

# A4.4. Alphakanal - Ein zusätzlicher Kanal zur Bildmaskierung
Falls in der Bilddatei ein Alphakanal enthalten ist, finden sich dort zusätzliche Informationen zur Transparenz. Der Alphakanal ist also ein zusätzlicher Kanal, der in Rastergrafiken die Durchsichtigkeit der einzelnen Pixel (Bildpunkte) speichert. Beim GIF-Format existiert zwar kein eigentlicher Alphakanal, man kann aber eine Farbe definieren, die transparent wirkt und z.B. auf Webseiten Grafiken mit geschwungenen oder runden Formen zulässt.
![Alphakanal](/GITressourcen/Daten_codieren/Alphakanal.jpg)
<br>

# A4.5. Unterschiede und Einsatzgebiete von Rastergrafiken und Vektorgrafiken
![RasterVektor](/GITressourcen/Daten_codieren/RasterVektor.jpg)
<br>

* **Rastergrafik, Bitmap oder Pixelmap:** Durch z.B. ein Foto-Objektiv auf einen CCD-Sensor (CCD = Charge Coupled Device) oder CMOS-Sensor (z.B. als APS = Active Pixel Sensor) eingefangene Bildpunkte. (CMOS ist übrigens die Bezeichnung für spezielle und nicht nur lichterfassende Halbleiterbauelemente = Complementary Metal Oxide Semiconductor)
* **Vektorgrafik:** Eine Vektorgrafik wird aus grafischen Primitiven wie Linien, Kreisen, Polygonen oder allgemeinen Kurven (Splines) beschrieben. Da die Vektorgrafik verlustlos skalierbar ist, kann sie im Gegensatz zur Rastergrafik ohne entstehende Artefakte beliebig vergrössert werden. Bei Schrift-Fonts (z.B. TTF = TrueType Font) handelt es sich übrigens um Vektorgrafiken. Die Vektorgrafik beschreibt ein Bild mathematisch und muss bei jeder Ausgabe auf einen Bildschirm oder einen Matrixdrucker wie Inkjet- oder Laserprinter Pixel für Pixel in eine Rastergrafik der gewünschten Auflösung (Breite x Höhe) umgerechnet (gerendert) werden. Anders sieht das beim Ausgabegerät Stiftplotter aus, wo ein in einer Ebene aufgehängter und in x-Achse und y-Achse verschiebbarer Zeichenstift die Grafik zu Papier bringt: Dieser versteht prinzipbedingt direkt Vektordaten.<br>
TTF: TrueType-Fontformat (.ttf)<br>
PostScript-Fontformate: Type-1-Schriften (.pfm)<br>
OpenType: TrueType- oder PostScript-flavoured (.ttf oder .otf)<br>
Fontsätzen: ARIAL, COURIER NEW, COMIC SANS MS, FUTURA, FRUTIGER, HELVETICA etc. *(Nicht alle Fontsätze beinhalten den kompletten ASCII- und schon gar nicht den vollständigen UTF8-Zeichensatz. Gewisse Fancy-Schriftsätze beschränken sich auf die Zeichen A-Z, 0-9, inklusive dem Bindestrich -)*
<br><br>

# A4.6. Wichtige Begriffe im Multimediabereich: DPI und PPI
Mit **dpi = Dots per Inch** meint man die Punktedichte bzw. Auflösung bei Druckern: Anzahl Farbpunkte pro Inch. (1 inch entspricht 2.54 cm). Zoll ist übrigens die deutsche Übersetzung für Inch. Gängige Druckerauflösungen starten bei 300dpi und gehen bis zu ca. 4000dpi. Die richtige Wahl der dpi's ist abhängig von der gewählten Papiergrösse und dem Verwendungszweck.<br>
Mit **ppi = Pixel per Inch** meint man die Pixeldichte bei Displays. Ein modernes Apple-Ipad besitzt bis zu ca. 260 ppi. Es stellt sich die Frage, ob das menschliche Auge in der Lage ist, mit seiner max. Winkelauflösung von einer Bogenminute eine solch hohe Auflösung bei kleinem Abstand zum Display auch aufzulösen. PC-Monitore werden üblicherweise nicht in ppi angegeben, sondern in Anzahl Pixel horizontal bzw. vertikal zusammen mit der Displaydiagonalen in Zoll oder Zentimeter.
<br><br>

# A4.7 Aufgabenblock 1: Bildbearbeitung und Transparenz
1. Suchen sie im Internet ein Bild vom Matterhorn, das eine Grösse von mind. 3000 Pixel aufweist und laden sie es auf ihren Notebook herunter. Danach bearbeiten sie es in einer Bildbearbeitungs-Software ihrer Wahl. Empfohlen wird das Online-Bildbearbeitungswerkzeug auf www.pixlr.com.<br>
Da das Bild eine viel zu hohe Auslösung hat, rechnen sie es herunter. Bei dieser Gelegenheit ändern sie das Bildseitenverhältnis auf 16:9. Sie werden sich für einen Bldauschschnitt entscheiden müssen. Das Bild soll schlussendlich 720 Bildzeilen ausweisen.<br>
Speichern sie das Bild als JPG in höchster und tiefster Qualität ab, zudem auch als PNG ohne Transparenz. Notieren sie sich die erfoderlichen Speichergrössen. Im Anschluss berechnen sie den unkomprimierten, theoretischen Speicherbedarf bei 8 Bit pro Farbkanal in MiB. Vergleichen sie die Werte und erklären sie die Unterschiede.

2. Nun erstellen sie aus demselben Matterhorn-Bild ein rundes Matterhorn-Logo: Schneiden sie das Matterhorn kreisförmig aus, damit eine Matterhorn-Medaille entsteht. Der Durchmesser soll 640 Pixel betragen. Beschriften sie das runde Bildchen mit dem Text "Matterhorn" und speichern sie es als PNG mit Transparenz ab. Suchen sie im Internet ein geeignetes Farbmuster, das als Hintergrundbild dienen soll. Laden sie das gewählte Bild auf Ihren Notebook herunter und bearbeiten sie es in ihrer Grafikapplikation wie folgt: Reduzieren sie die Farbsättigung derart, dass die Farben nur noch schwach angedeutet werden. Reduzieren sie auch den Kontrast. Speichern sie das Hintergrundbild in der Grösse 2000 Pixel x 2000 Pixel ab. Überprüfen sie ihr Werk in einer Webseite wie folgt:
```
<html>
  <head>
    <style>
      body {
        background-image: url('myBackground.jpg');
        background-repeat: no-repeat;
      }
    </style>
  </head>
  <body>
  <img src="Matterhorn.png">
  </body>
</html>
```

<br><br>

# A4.8 Aufgabenblock 2: Bild und Ton

1. Berechnen sie den **Speicherbedarf** für ein unkomprimiertes Einzelbild im HD720p50-Format bei einer True-Color-Farbauflösung. (Die Begriffe HD720p50 und TrueColor bitte googeln)

2. Und welchen **Speicherbedarf** hat das Video aus Aufgabe 1, bei einer Spieldauer von 3 Minuten?

3. Ihre **Digitalkamera** bietet für die Speicherung ihrer Bilder folgende Formate an: RAW, TIF, JPG. Erklären sie in ein paar kurzen Sätzen die Unterschiede und Einsatzgebiete dieser drei Formatvarianten.  

4. Sie möchten ihr neulich erstelltes Gameplay-Video auf **Youtube** veröffentlichen. Was sind die technischen Vorgaben dazu? (Format, Bildrate, Farbauflösung, Video-, Audiocodec etc.). Gibt es allenfalls rechtliche Einschränkungen?

5. Sie haben ein **30-Zoll-Display** (Diagonale) im Format 16:10 und 100ppi erworben. Was ist die Pixelauflösung horizontal und vertikal?

6. Sie **drucken** ein quadratisches Foto mit einer Kantenlänge von 2000 Pixel mit 600dpi. Wie gross in cm wird dieses? 

7. Ein kleiner Abstecher in die **Welt der Töne**:<br>
Bild-1 zeigt den Aufbau eines Mikrofons, Bild-2 den eines Lautsprechers. Was sind eigentlich, vom technischen Prinzip her, die Unterschiede? Erklären sie.<br>
Beides sind analoge Verfahren. Da der Computer nur "Digital" versteht, muss das analoge Signal zunächst in ein digitales Signal umgewandelt werden. Dafür sorgt ein sogenannter A/D-Wandler. Können sie seine Funktion in groben Zügen erklären? Bild-3 kann ihnen dabei weiter helfen.
![MikrophoneLautsprecher](/GITressourcen/Daten_codieren/AnalogDevice.jpg)

8. **Webseite** mit multimedialem Inhalt versehen:<br>
In dieser Übung geht es darum, auf einer Webseite Multimediainhalt korrekt zu integrieren.<br>
Als Starthilfe ist der folgende einfach gestrickte **HTML-Code index.html** inkklusive den darin verwendeten Medien gegeben:
```

    <!DOCTYPE html>
    <html lang="de">
      <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>WIREFIRE</title>
	    <style>
          body { background-color: #1c87c9; }
	      .content { max-width: 410px; margin: auto; }
        </style>
      </head>
      <body> <!-- Dokumentinhalt -->
	    <div class="content">
	      <img src="FirewireLogo.gif">
	      <img src="FirewireLogo.png">
	      <img src="tbztower.jpg" />
          <p style="font-family: Arial, Verdana">
	        Bildung kommt von Bildschirm und nicht von Buch, sonst hiesse es ja Buchung.
          </p>
	      <video controls poster="tbztower.png"> 
            <source src="tbztower.mp4">
          </video>
	    </div>
	  </body>
    </html>
```
Das Logo in zwei Ausführungen **FirewireLogo.gif** und **FirewireLogo.png**:<br>
![FirewireLogo.gif](/GITressourcen/Daten_codieren/web/FirewireLogo.gif)<br>
![FirewireLogo.png](/GITressourcen/Daten_codieren/web/FirewireLogo.png)<br>
Ein normales Farbbild **tbztower.jpg**:<br>
![tbztower.jpg](/GITressourcen/Daten_codieren/web/tbztower.jpg)<br>
Ein Film **tbztower.mp4**:<br>
![tbztower.mp4](/GITressourcen/Daten_codieren/web/tbztower.mp4)<br>
Ein Vorschaubild **tbztower.png** für den Film:<br>
![tbztower.png](/GITressourcen/Daten_codieren/web/tbztower.png)
<br>
Wenn sie wollen, können sie diese Webseite selber nachbauen. Dazu erstellen sie ein leeres Verzeichnis und kopieren alle hier angebotenen Dateien hinein. Rufen sie danach die HTML-Datei index.html auf. Es wird sich ihr Webbrowser melden und die Webseite anzeigen.<br>
**Frage:** Am oberen Rand der soeben erstellten Webseite erscheint das Logo FIREWIRE in zwei Ausführungen. Was sind die Unterschiede und woher führen diese? Für welche Logo-Variante werden sie sich bei ihrem Projekt entscheiden?
<br>
**Arbeitsauftrag:** Erstellen sie nun ihre eigene Webseite. Den HTML-Teil dürfen sie aus der Vorlage übernehmen, und allenfalls ausbauen oder abändern. Sie sollen aber unbedingt ihre eigene Mediendateien erstellen und anzeigen. Damit sind eigene, freigestellte Logos, Bild, Videos gemeint. Lassen sie ihrer Phantasie freien Lauf. Auch Audio oder Vektorgrafik (svg) ist denkbar. Wer von ihnen erstellt die originellste Webseite?<br>

<br><br>

Folgende **Opensource-Programme** können ihnen bei dieser Aufgabe nützlich sein:<br>
[Bildbearbeitungs-SW GIMP](https://www.gimp.org/)<br>
[Bildbearbeitungs-SW PIXLR](https://pixlr.com/)<br>
[Vektorgrafik-SW INKSCAPE](https://inkscape.org/de/)<br>









