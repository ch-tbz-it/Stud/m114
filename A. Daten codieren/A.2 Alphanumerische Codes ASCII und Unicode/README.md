# A.2 Alphanumerische Codes ASCII und Unicode
*Dauer: 2 Lektionen<br>
Autor: ARJ<br>
Letzte Änderung: 10.8.2022*

## Der ASCII-Code
Der ASCII-Code (American Standard Code for Information Interchange) ist in seiner Ursprungsversion eine 7-Bit-Zeichenkodierung und wurde bereits damals eingesetzt, wo noch Textnachrichten per Fernschreiber (Telex) übermittelt wurden. Die druckbaren Zeichen umfassen das lateinische Alphabet in Gross- und Kleinschreibung, die zehn arabischen Ziffern sowie einige Interpunktionszeichen (Satzzeichen, Wortzeichen) und andere Sonderzeichen. Der Zeichenvorrat entspricht weitgehend dem einer Tastatur oder Schreibmaschine für die englische Sprache. Die nicht druckbaren Steuerzeichen enthalten Ausgabezeichen wie Zeilenvorschub oder Tabulator, Protokollzeichen wie Übertragungsende oder Bestätigung und Trennzeichen wie Datensatztrennzeichen.<br>

## ASCII-Erweiterung gemäss ISO 8859
ISO: International Organization for Standardization / Internationale Organisation für Normung)
ASCII belegte bekanntlich ursprünglich 7 Bit pro Character (0...127) und wurde später um ein Bit auf 8 Bit (0...255) erweitert. Um den verschiedenen Sprachen gerecht zu werden, wurde der ISO-Standard 8859 definiert, der nun im zweiten Teil des ASCII-Zeichensatz (128...255) deren 16 Sprachzusätze unterscheidet.<br>
Bsp.: ISO-Standard 8859-1 = Latin-1, Westeuropäisch oder ANSI-ASCI.

**Auftrag:** Besorgen sie sich im Internet eine vollständige ASCII-Tabelle als Bild- oder PDF-Datei.

## Unicode
Der ASCII-Code mit seinen 256 Zeichen genügt den heutigen Anforderungen nicht mehr. Es fehlen z.B. die chinesische Schriftzeichen oder wie wär's mit einem Violinschlüssel? Es muss ein umfangreicherer Zeichencode her, nämlich der Unicode.

* Ein Unicode kann max. 8 Byte lang sein (64 Bit): U+XXXX'XXXX, wobei Unicode V2.0 bisher erst 1‘114‘112 verschiedene Zeichen U+0000'0000 bis U+0010'FFFF nutzt.
* UTF ist die verbreitetste Unicode-Kodierungsform (UTF=**U**niversal-Coded-Character-Set **T**ransformation **F**ormat)
* UTF-8: Je nach Zeichen beträgt die Codelänge von UTF-8 ein bis vier Bytes. UTF-8 ist in den ersten 128 Zeichen (Indizes 0–127) deckungsgleich mit ASCII.
* UTF-16: Je nach Zeichen beträgt die Codelänge von UTF-16 zwei oder vier Bytes. Zusätzlich muss mit der Byte-Order-Mark BOM (=Bytereihenfolge) angegeben werden, ob nach BigEndian BE oder LittleEndian LE verfahren wird. (Bsp. Datum : BE wäre yyyy.mm.dd, LE wäre dd.mm.yyyy)
* UTF-32: Ein einzelnes Zeichen belegt immer exakt 32 Bit.
* Notation bei HTML: \&\#x\<*unicode*\>; (hexadezimale Notation des Unicodes)
* Eingabe bei Windows-Word: U+\<*unicode*\> gefolgt von der Tastenkombination Alt+C.
* Wenn das Unicode-Zeichen im gewählten Font-Satz (Arial, CourierNew etc.) nicht implementiert ist, wird auf dem Bildschirm oder am Drucker ein Stellvertreter-Zeichen oder ein Leerzeichen dargestellt.

Hinweis: Notepad++ versteht nebst ANSI-ASCII auch UTF-8 und UTF-16 mit LE-BOM und BE-BOM.<br><br>

**Auftrag:** Suchen sie im Internet nach einer Webseite wo Unicode-Zeichen gelistet sind und merken sie sich den Link darauf.

## Aufgaben zu ASCII und Unicode
Sie erhalten drei Textdateien: [Textsample1](/GITressourcen/Daten_codieren/Textsample1), [Textsample2](/GITressourcen/Daten_codieren/Textsample2) und [Textsample3](/GITressourcen/Daten_codieren/Textsample3)<br>
Eine der Dateien ist in ASCII codiert, die andere in UTF-8 und die dritte in UTF-16. Beantworten sie nun die folgenden Fragen:

* Welche der Dateien ist nun ASCII-codiert, welche UTF-8 und welche UTF-16 BE-BOM?
* Alle drei Dateien enthalten denselben Text. Aus wie vielen Zeichen besteht dieser?
* Was sind die jeweiligen Dateigrössen? (Beachten sie, dass unter Grösse auf Datenträger jeweils 0 Bytes angegeben wird. Dies darum, weil beim Windows-Dateisystem NTFS kleine Dateien direkt in die MFT (Master File Table) geschrieben werden.) Wie erklären sie sich die Unterschiede?
* Bei den weiteren Fragen interessieren uns nur noch die ASCII- und die UTF-8-Datei: Bekanntlich ist UTF-8 in den ersten 128 Zeichen deckungsgleich mit ASCII. Untersuchen sie nun die beiden HEX-Dumps und geben sie an, welche Zeichen unterschiedlich codiert sind. Ein kleiner Tipp: Es sind deren zwei.
* Was bedeuten die beiden Ausdrücke, denen wir z.B. bei UTF-16 begegnen: Big-Endian (BE), Little-Endian (LE)?
* Im Notepad++ kann man unter dem Menüpunkt Codierung von ASCII zu UTF umschalten. Spielen sie damit etwas herum und notieren sie sich, was in der Darstellung jeweils ändert.
* Für Anspruchsvolle: Der UTF-8-Code kan je nach Zeichen ein, zwei, drei oder vier Byte lang sein. Wie kann der Textreader erkennen, wann ein UTF-8 Zeichencode beginnt und wann er endet? Untersuchen sie dies anhand der beiden Textsamples und lesen sie in z.B. Wikipedia die entsprechende Theorie zu UTF-8 durch. Tipp: Startbyte und Folgebyte.


