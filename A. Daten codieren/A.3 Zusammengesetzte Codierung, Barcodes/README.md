# A.3 Zusammengesetzte Codierung, Barcodes
*Dauer: 2 Lektionen<br>
Autor: ARJ<br>
Letzte Änderung: 30.5.2023*

## Zusammengesetzte Codierung
Mit zusammengesetzter Codierung ist ein Datensatz (Record, Tupel etc.) gemeint. Ein Datensatz ist eine Gruppe von inhaltlich zusammenhängenden und zu einem Objekt gehörenden Datenfeldern. (Z.B. Artikelnummer, Artikelname, Farbe, Länge, Breite etc.) Datensätze entsprechen einer logischen Struktur, die bei der Softwareentwicklung (Datenmodellierung) festgelegt wurde. In der Datenverarbeitung werden zu Datensätzen zusammengefasste Daten in Datenbanken oder in Dateien gespeichert. Sie sind Gegenstand der Verarbeitung von Computerprogrammen und werden von diesen erzeugt, gelesen, verändert und gelöscht.

## Barcodes
![Barcode](/GITressourcen/Daten_codieren/Barcode.jpg)

* EAN-13: Das Bildchen mit den verschiedenbreiten schwarzen und weissen Balken, wie man es heutzutags auf allen Food- und Non-Food-Artikeln antrifft, repräsentiert eine 13-stellige Zahl. Diese 13 Zahlen sind vom Produktehersteller oder einer Organisation weiter aufgeschlüsselt wie z.B. in Ländercode, Produktcode, Lotnummer, Datum, Prüfziffer etc.
* QR-Code: Der QR-Code ist im Gegensatz zum EAN-13-Code ein zweidimensionaler Code. Dank einer "eingebauten" Fehlerkorrektur können bis zu 30% der QR-Grafik beschädigt oder verschmutzt sein, ohne die Lesbarkeit zu beinträchtigen (gilt für Fehlerkorrektur-Level H). Diese Eigenschaft wird oft zur Plazierung von Werbebildchen und Logos missbraucht. Der maximale Informationsgehalt (177×177 Elemente, max. Verlust von 7% der Daten bzw. Fehlerkorrektur-Level L) beträgt knapp 3kB. Das würde dann ca. 7000 Dezimalziffern oder 4300 alphanumerische Zeichen ergeben. Vorsicht: Weil der Inhalt eines QR-Codes nicht auf den ersten Blick ersichtlich ist, kann man einem Fake-Link auf schädliche Webseiten aufsitzen oder das Smartphon führt Malware aus.

## Auftrag zu zusammengesetzter Codierung und Barcodes

1. Einfacher QR-Code erstellen und lesen:<br>
Denken sie sich eine kurze Botschaft, URL etc. aus und bilden sie diese Information in einem QR-Code ab. Danach tauschen sie mit ihrem Banknachbar/in ihre QR-Codes aus. Wenn der QR-Code gelesen werden kann, waren sie erfolgreich. Applikationen die alphanumerischen Text in einen QR-Code und zurück wandeln, findet man im Internet.

2. Zusammengesetzte Daten in einem QR-Code:<br>
Ihre Firma wird mit dem Design von Tickets für ein Fussballstadion beauftragt. Das Eintrittsticket soll mit einem QR-Code versehen sein, der alle wichtigen Informationen zur Buchung enthält. Dies soll ermöglichen, das Ticket jederzeit und überall von Offline-QR-Readern lesen und überprüfen zu lassen bzw. Zugang zu den Satdionbereichen zu gewähren. Das Ticket soll die folgenden, codierten Informationen enthalten:<br><br>
X Datum und Uhrzeit der Veranstaltung<br>
X Fortlaufende, alphanumerische Ticketnummer (Ticketnummern seit Tag-0)<br>
X Numerische Sitzplatznummer (Jeder Sitzplatz hat eigene Nummer)<br>
X Tribünensektor A-Z (Für einfache Platzeinweisung der Besucher)<br>
X ID- oder Passnummer des Besuchers (Tickets nicht übertragbar)<br><br>
Überlegen sie sich, ob das Ticket fälschungssicher ist. Braucht es allenfalls noch eine Prüfziffer, Checksum etc.<br>
Wenn der Besucher am Stadioneingang erscheint, zeigt er sein Ticket. Dieses wird von einem Platzanweiser mit einem QR-Code-Leser gelesen. Damit kann er die Personalie und Gültigkeit des Tickets überprüfen und den Besucher anschliessend in den richtigen Stadionsektor leiten.<br>
Bilden sie diesen Datensatz in einen QR-Code ab. Erstellen sie QR-Codes für fiktive Veranstaltungen. Testen sie ihre QR-Codes mit ihrem Banknachbarn/in gegenseitig aus.
Applikationen die alphanumerischen Text in einen QR-Code und zurück wandeln, findet man im Internet.
Auf Komplettlösungen aus dem Internet - es gibt zu diesem Thema mehr oder weniger "pfannenfertige" Applikationen - bitte aber verzichten.
