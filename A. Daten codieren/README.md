# A. Daten codieren
Für dieses Kapitel stehen an der Schule insgesamt 12 Lektionen zu je 45 Minuten zur Verfügung.

- A.1 Zahlensysteme, numerische Codes (Unsigned und Signed)<br>
Dauer: 4 Lektionen<br>
[Hier geht's zum Unterrichtsmaterial](/A. Daten codieren/A.1 Zahlensysteme, numerische Codes)
- A.2 Alphanumerische Codes ASCII und Unicode<br>
Dauer: 2 Lektionen<br>
[Hier geht's zum Unterrichtsmaterial](/A. Daten codieren/A.2 Alphanumerische Codes ASCII und Unicode)
- A.3 Zusammengesetzte Codierung, Barcodes<br>
Dauer: 2 Lektionen<br>
[Hier geht's zum Unterrichtsmaterial](/A. Daten codieren/A.3 Zusammengesetzte Codierung, Barcodes)
- A.4 Bildcodierung (Bildpixel Farbsysteme und Farbcodierung RGB, CMYK, YCrCb , Bildformate, Unterschied Rastergrafik (Bitmap) zu Vektorgrafik. Übersicht über die wichtigsten Multimediaformate für Bild, Audio, Video wie z.B. TIF, JPG, GIF/PNG, MOV, h.264/MPEG4, WAV, MP3/AAC, PDF, EPS)<br>
Dauer: 4 Lektionen<br>
[Hier geht's zum Unterrichtsmaterial](/A. Daten codieren/A.4 Bildcodierung)

