# A. Zahlensysteme, numerische Codes
*Dauer: 4 Lektionen<br>
Autor: ARJ<br>
Lernziele: Bit/Byte; Massvorsätze k,M,G,T; Zahlensysteme BIN/HEX; 2er Komplement; Dataoverflow; Fliesskommazahl; Parallel; Seriell; Bustakt Hz<br>
Letzte Änderung: 14.11.2022*

## Zahlensysteme - Reloaded
Zahlensysteme wurden im Fach Mathematik ausführlich behandelt. Hier können sie nochmals ihre Kenntnisse der vier für die Informatik relevanten Zahlensysteme auffrischen: 

* **Zweiersystem oder Binärsystem bzw. Dualsystem**<br>
Basis: 2<br>
Zeichenvorrat: 0, 1<br>
Anwendung: Mikroprozessoren rechnen im Binärsystem. Die kleinste Einheit nennt man Bit. Ein Bit kann zwei Zustände annehmen: 0 oder 1 (bzw. Wahr oder Falsch, Nordpol oder Südpol, Hell oder Dunkel usw.) 8 Bits entsprechend einem Byte und 16 Bit einem Word.<br>
Es gilt folgende Abmachung: b (kleines b) bedeutet Bit und B (grosses B) bedeutet Byte.
LSB bedeutet "Least Significant Bit" und damit ist das kleinstwertigste Bit gemeint.<br>
MSB bedeutet "Most Significant Bit" und damit ist das höchstwertigste Bit gemeint.
Die Beschriftung der LSB- bzw. MSB-Leitung ist z.B. bei Parallelverbindungen wichtig, damit ein Stecker nicht falsch herum angeschlossen wird.
* **Achtersystem oder Oktalsystem**<br>
Basis: 8<br>
Zeichenvorrat: 0, 1, 2, 3, 4, 5, 6, 7, 8<br>
Anwendung: Zugriffsrechte in UNIX/LINUX owner:rwx group:rwx other:rwx. Jeweils 3 Bit oder eine Oktalzahl.

> **Exkurs: Die Berechtigungen unter UNIX/LINUX:**<br>
Der Zugriff auf eine Datei (File) oder Verzeichnis (Directory) erfolgt "lesend=read=r" und/oder "schreibend=write=w" und/oder "ausführend=execute=x".<br>
Der Zugriff auf eine Datei oder Verzeichnis ist für drei Benutzerkreise definiert: Für den "Eigentümer=Owner", der Eigentümer-"Gruppe=Group" und für alle "Anderen=Rest_der_Welt=Others".<br>
Für jeden dieser Benutzerkreise wird der Zugriff wie folgt definiert: Owner:rwx Group:rwx Others:rwx. wobei für r, w und x entweder eine logische 0 (=verweigerte Rechte), oder eine logische 1 (=erteilte Rechte) steht.<br>
Dazu ein Beispiel: Die Datei README.TXT gehört dem User Felix und der Gruppe Benutzer und soll für Felix lesend und schreibend zugreifbar sein, für die Gruppe Benutzer nur lesend und alle anderen sollen keinen Zugriff darauf haben. Das sähe dann so aus: README.TXT 110 100 000. Oder in Oktalschreibweise: README.TXT 640.
Der UNIX/LINUX-Befehl, um Rechte zu ändern lautet übrigens "chmod". Die UNIX/LINUX-Befehlszeile, um der Gruppe ebenfalls Schreibrechte zu erteilen, könnte so aussehen: "chmod 660 README.TXT"

* **Zehnersystem oder Dezimalsystem**<br>
Basis: 10<br>
Zeichenvorrat: 0, 1, 2, 3, 4, 5, 6, 7, 8, 9<br>
Anwendnung: Gemessen, gewogen, gezählt und bezahlt wir normalerweise im Dezimalsystem. In der Netzwerktechnik werden IPv4-Adressen im Zehnersystem angegeben. Im weiteren sind die Subnetzmasken bei IPv4 in dezimaler Schreibweise angegeben, so wie auch die Subnetzmasken in CIDR-Notation bei IPv4 und IPv6.
* **Sechzehnersystem oder Hexadezimalsystem (HEX)**<br>
Basis: 16<br>
Zeichenvorrat: 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, A (=10), B (=11), C (=12), D (=13), E (=14), F (=15)<br>
Anwendnung: MAC-Adressen, IPv6-Adressen, Speicher- und Dateianalyse mit HEX-Editor, Programmierung<br>
Eine Hex-Ziffer entspricht einer 4-stelligen Dualzahl, oder anders ausgedrückt: Eine HEX-Ziffer entspricht 4 Bit.<br>
Hinweis: Die Umrechnung Binär zu HEX ist besonders eeinfach, wenn man sich daran erinnert, dass eine HEX-Ziffer 4 Bit entspricht: HEX-Zahl 

## Aufgaben zu Zahlensystemen und numerischen Codes
*(Zu diesen Aufgaben existieren keine Musterlösungen. Notieren sie sich ihre Fragen und besprechen sie diese anschliessend mit ihrer Lehrperson.)*

1. Ergänzen sie die folgende BIN-DEC-HEX-Zahlentabelle: (Was bedeutet MSB bzw. LSB?)

|BIN(MSB)|BIN|BIN|BIN(LSB)|DEC|HEX|
|:---|:---|:---|:---|---:|---:|
| 0 | 0 | 0 | 0 | 0 | 0 |
| 0 | 0 | 0 | 1 | 1 | 1 |
| 0 | 0 | 1 | 0 | 2 | 2 |
|   |   |   |   |   |   |
|   |   |   |   |   |   |
|   |   |   |   |   |   |
| 1 | 1 | 1 | 1 | 15| F |
2. Wandeln sie die folgende Dezimalzahl ohne Taschenrechner in die entsprechende Binärzahl um: 911
3. Wandeln sie die folgende Binärzahl ohne Taschenrechner in die entsprechende Dezimalzahl um: 1011'0110
4. Wandeln sie die folgende Binärzahl ohne Taschenrechner in die entsprechende Hexadezimalzahl um: 1110'0010'1010'0101
5. Der Addierer einer ALU erhält für Zahl-A: 1101'1001 und für Zahl-B: 0111'0101. Was wird man als Resultat erhalten? Erklären sie!
![ADD](/GITressourcen/Daten_codieren/ADD_1.jpg)
6. Sie analysieren mit den Network-Sniffer Wireshark ihren **Network-Traffic**. Sie haben in OSI-Layer3 unter anderem folgende Bitkombination herausgelesen:<br>
1100 0000 . 1010 1000 . 0100 1100 . 1101 0011<br>
Was bedeuet dies? Beantworten sie diese Frage möglichst umfassend.<br>
*(BTW: Wer für diese und alle weiteren Umrechnungen den Taschenrechner benutzt, betrügt sich selbst ;-)*
7. Nun wurde ihre Neugier geweckt und sie untersuchen erneut ihr **Netzwerk** mit Wireshark, diesmal aber den  OSI-Layer2 und finden unter anderem folgende Bitkombination:<br>
1011 1110 - 1000 0011 - 1000 0101 - 1101 0101 - 1110 0100 - 1111 1110<br>
Was bedeuet dies? Beantworten sie diese Frage möglichst umfassend.
8. In einer **LINUX-Shell** (Terminal) entdecken sie folgende Programmzeile:<br>
chmod 751 CreateWeeklyReport<br>
Was bedeuet dies? Beantworten sie diese Frage möglichst umfassend.
9. Dimensionieren sie für den Matterhorn-Express, wo insgesamt 107 Gondeln die Touristen von Zermatt auf den Trockenen-Steg befördern, die **Codebreite des Binärcodes** für die Kabinenzählung.<br>
Und wenn sie schon dabei sind, gleich noch dies: Auf dieselbe Art möchte der Betreiber die Anzahl Touristen pro Tag ermitteln. Die Bahn ist täglich maximal 12 Stunden in Betrieb und die Förderleistung der Bahn wird mit maximal 2800 Personen pro Stunde angegeben. 
10. In der **Arithmetisch-Logischen Unit** (ALU) ihres 8Bit-Mikrokontrollers steht im Register-1 (8 Bit-Speicherzelle) der binäre Wert 1001 1110 und im Register-2 (8 Bit-Speicherzelle) der binäre Wert 1100 1101. Beim nachfolgenden Ausführungsschritt sollen die beiden Werte mathematisch addiert (ADD) werden und das Resultat in Register-3 (8 Bit-Speicherzelle) gespeichert werden. Welche Bitkombination erwarten sie nach der Durchführung der ADD-Operation im Register-3? Begründen sie ihre Antwort.
11. Ihr Arbeitsspeicher, der übrigens ähnlich aufgebaut ist, wie der Autosilo aus Aufgabe 16, hat eine Kapazität von 4kiB. Wie viele Bit werden darin gespeichert? (Hinweis: 1kiB=1024B)
12. Sie untersuchen einen **Arbeitsspeicher** mit 12-Bit-Adress- bzw. 16-Bit-Datenbus.<br>
Welche Speicherkapazität in kiB besitzt dieser?<br>
Wie lautet die Speicheradresse des ersten Datenbytes und wie die Speicheradresse des letzten Datenbytes?
![Speicher](/GITressourcen/Daten_codieren/Speicheradressen.jpg)
13. Zwei Geräte sind mit einer seriellen Leitung und zusätzlichem Taktsignal verbunden. Das Taktsignal beträgt 1MHz.<br>
Wieviele Bytes können damit pro Sekunde übertragen werden?<br>
Wieviele Bytes pro Sekunde könnten übertragen werden, wenn die Verbindung der beiden Geräte nicht seriell, sondern 8 Bit-parallel wäre?
![Takt](/GITressourcen/Daten_codieren/Takt.jpg)
14. Bei den bisherigen Aufgaben handelte es sich um Binärcodes, die positive, numerische Werte repräsentierten. Bei den Programmiersprachen spricht man vom Datentyp "Unsigned Integer". Hin und wieder möchte man aber auch die **negative Zahlenwelt** miteinbeziehen, man nennt dies dann "Signed Integer", was mit vorzeichenbehaftete Ganzzahl übersetzt werden kann.<br>
*(Wobei man gut beraten ist, abzuwägen, ob der Mehraufwand für negative Zahlen gerechtfertigt ist, wie folgendes Beispiel aus der Physik zeigt: Die Celsius-Teperaturskala kennt negative Werte, die Kelvin-Temperaturskala hingegen nicht und beide Skalen messen dieselbe Temperatur. Zum Beispiel entspricht der absolute Temperatur-Nullpunkt (nichts kann kälter sein, dass ist in diesem Fall entscheidend) -273 Grad Celsius aber eben auch 0 Grad Kelvin, Wasser schmilzt bei 0 Grad Celsius bzw. 273 Grad Kelvin und der heutige Sommer bot angenehme 35 Grad Celsius oder eben 308 Grad Kelvin.)*<br>
Möchte man den binären Zahlenstrahl in den negativen Bereich erweitern, liegt die Versuchung nahe, das erste Bit (MSB) als Vorzeichen zu verwenden. Funktioniert leider nicht bzw. nicht in allen Fällen! Die Lösung die funktioniert, nennt man **Zweierkomplement**. Was darunter zu verstehen ist, wird im Internet unzählige Male erklärt. Darum spare ich mir das hier.<br>
Nun zur Aufgabe: Wir gehen von einer Verarbeitungsbreite von einem Byte aus. (Datenbusbreite:1Byte)<br>
a. Nennen sie kleinster und grösster Binärwert bzw. Dezimaläquivalent im Falle von Unsigned bzw. Vorzeichenlos.<br>
b. Nennen sie kleinster und grösster Binärwert bzw. Dezimaläquivalent im Falle von Signed bzw. Vorzeichenbehaftet.<br>
c. Wandeln sie die Dezimalzahl +83 in einen vorzeichenbehafteten Binärwert um. (Signed)<br>
d. Wandeln sie die Dezimalzahl -83 in einen vorzeichenbehafteten Binärwert um. (Signed)<br>
e. Wandeln sie die Dezimalzahl 0 in einen vorzeichenbehafteten Binärwert um. (Signed)<br>
f. Addieren sie mathematisch den erhaltenen Binärwert aus Teilaufgabe 14.c und Teilaufgabe 14.d zusammen.<br>
g. Warum können sie bei der gegebenen Datenbusbreite von 1 Byte die Dezimalzahl +150 nicht in einen vorzeichenbehafteten Binärwert umwandeln? Begründen Sie und ziehen sie daraus ihre Lehren für einen zukünftigen Programmiersprachenkurs.
15. Last but not least: Bisher haben wir immer von ganzen Zahlen gesprochen. Oft genügt das in der Welt der Zahlen aber nicht. Dazu ein Beipiel: Teile ich die Ganzzahl 1 durch die Ganzzahl 3 und multipliziere sie darauf wieder mit der Ganzzahl 3 erhalte ich (sofern der Compiler/Interpreter nicht trickst) die Ganzzahl 0, was nachweislich falsch ist. Dies weil das Resultat der Division nicht als 0.3333333 sondern als ganze Zahl 0 (Nachkommastellen werden ignoriert) zwischengespeichert wird. Benötigt wird also ein Datentyp, der mit **Fliesskommazahlen** (Floating Point Numbers) klarkommt. Wie würden sie eine solche Fliesskommazahl definieren, und wie sie digital abspeichern? Machen sie dazu einen Vorschlag.
16. Zusatzaufgabe: Alles schon erledigt und latent etwas unterfordert? Dann dürfen sie nun etwas kreativ werden. Dieser **Autosilo** soll eine Fahrzeug-Liftanlage erhalten, die wie folgt funktioniert: Nachdem der Autofahrer sein Fahrzeug auf der Laderampe deponiert hat, wählt er einen freien Parkslot 1 bis 10 aus. Die freien Parkslots werden durch eine blinkende Lampe angezeigt. Danach verstaut die Liftsteuerung das Fahrzeug an der gewählten Stelle. Wünscht der Kunde sein Fahrzeug zurück, drückt er am Eingabeterminal die Zahl seines Parkslots und die Liftsteuerung erledigt den Rest bzw. deponiert sein Fahrzeug wieder an der Ausgabestelle. Wie würden sie dies realisieren? Machen sie sich ein paar Gedanken dazu.
![Autosilo](/GITressourcen/Daten_codieren/Autosilo.jpg)

## HEX-Editor und Notepad++
Im folgenden werden zwei für uns wichtige Werkzeuge behandelt, die sie auf ihrem Notebook, bei Sicherheitsbedenken in eine VM, installieren sollen:

* **HEX-Editor HxD**<br>
Unter einem HEX-Editor versteht man ein Computerprogramm, mit dem sich die Bytes beliebiger Dateien als Folge von Hexadezimalzahlen darstellen und bearbeiten lassen. Alternativ findet man im Internet auch einige Online-HEX-Editoren. Die HEX-Editor-App **HxD** findet man unter dem folgenden Link: 
[https://mh-nexus.de/de/hxd/](https://mh-nexus.de/de/hxd/)

* **Notepad++**<br>
Notepad++ ist ein freier Texteditor für Windows und kompatible Betriebssysteme und dem Standard-Texteditor von Windows eindeutig überlegen. Als Zeichensätze werden ASCII und verschiedene Unicode-Kodierungen unterstützt. **Notepad++** findet man unter dem folgenden Link:
[https://notepad-plus-plus.org/](https://notepad-plus-plus.org/)

## Aufgabe zu Notepad++ und HxD
Sie erhalten folgende Binärdatei: [switzerland.bin](/GITressourcen/Daten_codieren/switzerland.bin). Laden sie diese auf ihren Notebook und untersuchen sie es mit Notepad++ und HxD. In dieser Datei verstecken sich 4 vorzeichenlose 16-Bit-Ganzzahlen. Jede davon hat einen Bezug zur Schweiz. Wandeln sie die vier Zahlen je in ihr Dezimaläquivalent um und geben sie einen Tipp ab, was deren CH-Bedeutung sein kann. 








