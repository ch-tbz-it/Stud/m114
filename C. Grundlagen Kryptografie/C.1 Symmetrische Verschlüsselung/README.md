# C.1 Symmetrische Verschlüsselung
*Dauer: 2 Lektionen<br>
Autor: ARJ<br>
Letzte Änderung: 19.6.2023*

## Cryptool-SW installieren
Mit CrypTool1, einem Open-Source-Projekt und somit freier Lern-Software erhalten wir ein Tool in dei Hand, das die Konzepte der Kryptographie und der Kryptoanalyse erfahrbar machen lässt. Installieren Sie nun CrypTool1 auf ihrem Notebook, bei Sicherheitsbedenken als virtuelle Applikation. [**CRYPTOOL1** kann man hier herunterladen.](https://www.cryptool.org/de/ct1)

## Klassische Verfahren (Auswahl)
Symmetrische Verschlüsselungsverfahren zeichnen sich dadurch aus, dass der Absender die Botschaft mit demselben Schlüssel verschlüsselt, wie der Empfänger, der die Botschaft wieder entschlüsslet. Schon der römische Feldherr und spätere Kaiser Julius Cäsar kannte einen der folgenden Verschlüsselungstrick und nutzte ihn bei seinen geheimen Botschaften.

* **Die Rotationschiffre ROT:** Bei der Rotationschiffre (Verschiebechiffre) wird jeder Buchstabe des lateinischen Alphabets durch den im Alphabet um eine bestimmte Stellen davor bzw. dahinter liegenden Buchstaben ersetzt. Bei der ROT-13 wird z.B. um 13 Buchstaben verschoben:
![ROT](/GITressourcen/Daten_verschlüsseln/ROT.jpg)
* **Die Vigenèreverschlüsselung:** Bei monoalphabetischen Chiffrierverfahren wie der Rotationschiffre wird ein Buchstabe immer durch denselben Buchstaben ersetzt. Darum sind derart verschlüsselte Geheimtexte schnell nicht mehr geheim, weil sie mit einer einfachen Häufigkeitsanalyse leicht entschlüsselbar sind. Der nun vorgestellte Algorithmus ist ein einfaches polyalphabetisches Chiffrierverfahren. Beim polyalphabetisches Chiffrierverfahren wird ein Buchstabe in der Regel mit verschiedenen Buchstaben chiffriert. Ein einfaches polyalphabetisches Chiffrierverfahren wurde von Blaise de Vigenère (1523-1596), basierend auf den Ideen eines Benediktinermönches, entwickelt. Das nach ihm benannte Vigenère-Verfahren galt lange Zeit als unknackbar:
![Vigenere](/GITressourcen/Daten_verschlüsseln/Vigenere.jpg)
Zur Verschlüsselung eines Klartextes wird ein Schlüssel benötigt. Idealerweise ist dieser gleich lang wie der Klartext. Ist der Schlüssel kürzer als der Klartext, wird dieser einfach wiederholt. Die chiffrierten Buchstaben ermittelt man mit dem Vigenère-Quadrat, einer quadratischen Anordnung von untereinander stehenden verschobenen Alphabeten. Dazu sucht man den Kreuzungspunkt der durch den jeweiligen Schlüsselbuchstaben gekennzeichneten Zeile und der Spalte des Quadrats, die oben durch den Klartextbuchstaben gekennzeichnet ist. Der Empfänger benutzt zum Entschlüsseln der Nachricht denselben Schlüssel wie der Absender. Er kann durch Umkehren des Verschlüsselungsverfahren den Klartext zurückgewinnen.

## Aktuelle Verfahren (Auswahl)
Der Einsatz von elektrischen Hilfsmittel machte wesentlich komplexere Verfahren möglich.

* **Die XOR-Stromchiffre:** Bei der Stromchiffre werden Klartext Bit für Bit bzw. Zeichen für Zeichen XOR-ver- bzw. entschlüsselt. Sender und Empfänger benutzen denselben Schlüssel.
![XOR](/GITressourcen/Daten_verschlüsseln/XOR.jpg)
Ein grosser **Nachteil** der XOR-Verschlüsselung ist der, dass wenn Original und Chiffre bekannt sind, der Schlüssel ohne weiteres rekonstruiert werden und für eine weitere Dechiffrierung verwendet werden kann.<br><br>
* **AES (Advanced Encryption Standard):** Modernes, symmetrisches Verschlüsselungsverfahren, dass z.B. bei PGP zusammen mit dem asynchron verschlüsselten Schlüsseltauch RSA Verwendung findet. Eine ausführliche Beschreibung findet man z.B. auf Wikipedia oder im Crypttool. Öffnen sie nun in der Cryptool-Onlineversion die folgende Visualisierung und studieren sie diese: 
[AES-Rijndael-Animation](https://www.cryptool.org/de/cto/aes-animation)<br><br>

## Aufgaben zur symmetrischen Verschlüsselung

1. **Die Rotationschiffre:**<br>
Schon der römische Feldherr und spätere Kaiser Julius Cäsar kannte den folgenden Verschlüsselungstrick und nutzte ihn bei seinen geheimen Botschaften: Ersetze jeden Buchstaben durch den, der eine bestimmte Anzahl Stellen später im Alphabet folgt! Somit konnte Cäsar effektiv geheime Botschaften übermitteln, wie z.B. diese Zitate:<br>
GHU DQJULII HUIROJW CXU WHHCHLW GLH ZXHUIHO VLQG JHIDOOHQ LFK NDP VDK XQG VLHJWH WHLOH XQG KHUUVFKH<br>
Benutzen Sie nun Ihr CrypTool1 und finden Sie heraus, um welche Zitate es sich handelt! Die Rotationschiffre ist übrigens ein klassisches, symmetrisches Verfahren. Nun aber nicht einfach drauflos probieren. Machen Sie etwas Kryptoanalyse mit einem ASCII-Histogramm. *(Tipp: Häufigkeitsanalyse der vorkommenden Buchstaben)*<br><br>
2. **Vigenèreverschlüsselung:** <br>
Um etwas warm zu laufen, verschlüsseln wird ohne Cryptool (!) das Wort BEEF mit dem Schlüsselwort AFFE.<br><br>
Danach, wiederum ohne Cryptool, entschlüsseln wir den Geheimtext WRKXQT mit dem Schlüsselwort SECRET.<br><br>
Nun wirds wirklich spannend: Wir versuchen den Vigenère-Code zu knacken und bedienen uns einem Analysewerkzeug im Cryptool1. Heimlich abgehört haben wir die folgende Vigenère-Chiffre:<br>
USP JHYRH ZZB GTV CJ WQK OCLGQVFQK GAYKGVFGX NS ISBVB MYBC MWCC NS JOEVB GTV KRQFV AGK XCUSP VFLVBLLBE ESSEILUBCLBXZU SENSWFGVRCES SER CZBCE ILUOLBPYISL CCSZG VZJ<br>
Neugierig wie wir sind, möchten wir gerne wissen, welcher Text hinter dieser Chiffre steckt. Da uns aber das Schlüsselwort fehlt, müssen wir tief in unsere Trickkiste greifen. *(Tipp: Im CrypTool1/Hilfe/Index/Vigenère-Verschlüsselungsverfahren findet man weitere Informationen zum Vigenère-Analyseverfahren.)*<br><br>
Zu guter Letzt versuchen wir, ob das Analysetool auch Resultate liefert, wenn das Passwort wesentlich länger ist. 
Nehmen sie den entschlüsselten Text von vorhin und verschlüsseln sie ihn erneut, diesmal aber mit diesem Schlüssel:<br><br>
LoremipsumdolorsitametconsectetueradipiscingelitAeneancommodoligulaegetdolorAeneanmassaCumsociisnatoquepenatibusetmagnisdisparturientmontesnasceturridiculusmusDonecquamfelisultriciesnecpellentesqueeupretiumquissemNullaconsequatmassaquisenimDonecpedejustofringillavelaliquetnecvulputateegetarcuInenimjustorhoncusutimperdietavenenatisvitaejustoNullamdictumfeliseupedemollispretiumIntegertinciduntCrasdapibusVivamuselementumsempernisiAeneanvulputateeleifendtellusAeneanleoligulaporttitoreuconsequatvitaeeleifendacenimAliquamloremantedapibusinviverraquisfeugiatatellusPhasellusviverranullautmetusvariuslaoreetQuisquerutrumAeneanimperdietEtiamultriciesnisivelaugueCurabiturullamcorperultriciesnisiNamegetduiEtiamrhoncusMaecenastempustellusegetcondimentumrhoncussemquamsemperliberositametadipiscingsemnequesedipsumNamquamnuncblanditvelluctuspulvinarhendreritidloremMaecenasnecodioetantetincidunttempusDonecvitaesapienutliberovenenatisfaucibusNullamquisanteEtiamsitametorciegeterosfaucibustinciduntDuisleoSedfringillamaurissitametnibhDonecsodalessagittismagnaSedconsequatleoegetbibendumsodalesauguevelitcursusnunc<br><br>
Funktionieren nun die Vigenere-Analysetools immer noch?<br><br>
3. **XOR-Stromchiffre:**<br>
Verschlüsseln sie die Dezimalzahl 4711 von Hand als XOR-Stromchiffre. Der binäre Schlüssel lautet: 1000'1101. Zur Kontrolle entschlüsseln sie die erhaltene Chiffre wieder.
Hinweis: Sie müssen die Dezimalzahl zuerst in eine 16-Bit Binärzahl umwandeln (Führende Nullen nicht weglassen). Sollte der Schlüssel für die Verschlüsselung zu kurz sein, wird dieser mehrmals wiederholt. Der Datenstrom soll in dieser Aufgabe mit der Übertragung des MSB's, also von links nach rechts beginnen.<br><br>

4. **Sicheres Passwort**<br>
Da Cryptool ja bereits geöffnet ist, kann es auch nicht schaden, mal sein Lieblingspasswort auf seine Sicherheit zu überprüfen. Crypttol bietet dazu einen Passwort-Qualitätsmesser an:<br>
Einzelverfahren/Tools/Passwort-Qualitätsmesser

