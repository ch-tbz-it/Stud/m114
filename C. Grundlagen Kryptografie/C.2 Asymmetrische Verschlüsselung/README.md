# C.2 Asymmetrische Verschlüsselung
*Dauer: 3 Lektionen<br>
Autor: ARJ<br>
Letzte Änderung: 14.8.2022*

## Cryptool-SW installieren
Falls noch nicht geschehen, bitte jetzt CrypTool1 installieren. [**CRYPTOOL1** kann man hier herunterladen.](https://www.cryptool.org/de/ct1)


## Das Konzept
![PPG](/GITressourcen/Daten_verschlüsseln/PGP.jpg)

## Der Ablauf
![RSA](/GITressourcen/Daten_verschlüsseln/RSA.jpg)

## Wie sicher ist das Verfahren grundsätzlich?
Der Einstieg in die asymmetrischen Verschlüsselung erfolgte um das Jahr 1976 durch die Entwicklung eines Protokolls für eine sichere Schlüsselvereinbarung durch die beiden Kryptologen Whitfield Diffie und Martin Hellman. Zwei Kommunikationspartner konnten nun über eine öffentliche, abhörbare Leitung einen gemeinsamen, geheimen und von Drittpersonen nicht berechenbaren Schlüssel in Form einer Zahl vereinbaren. Dieser gemeinsame Schlüssel konnte anschliessend für eine symmetrische Datenverschlüsselung verwendet werden (z.B. DES Data Encryption Standard oder AES Advanced Encryption Standard).<br><br>
Die Grundidee dieses Diffie-Hellman-Schlüsseltauschs soll nun anhand einer Farb-Analogie bzw. mittels Farbmischung erklärt werden. 
Das Farbenmischen wird hier als eine Einwegfunktion aufgefasst. Damit meint man, das es einfach ist, zwei oder mehrere verschiedene Farben zusammenzuschütten. Wesentlich schwieriger ist es allerdings, die erhaltene Farbmischung wieder in ihre ursprünglichen Komponenten aufzuteilen. (Umkehrung)
![Diffie](/GITressourcen/Daten_verschlüsseln/Diffie.jpg)

* Alice und Bob einigen sich öffentlich auf eine gemeinsame Farbe (Gelb).
* Jeder wählt für sich eine eigene, geheime Farbe (Alice: Orange, Bob; Türkis).
* Bob und Alice mischen nun jeweils die gemeinsame Farbe (Gelb) mit ihrer geheimen Farbe (Orange/Türkis). Alice erhält die Farbe Beige und Bob Graublau.
* Diese Farbmischungen tauschen Alice und Bob nun aus. Da darf jeder zuschauen. Diese beiden Farbmischungen sind nämlich nicht geheim. Für einen Aussenstehendne ist es nicht effizient möglich, aus den «öffentlichen» Farben (Gelb, Beige, Graublau) auf die geheimen Farben von Alice und Bob zu schliessen.
* Nun mischen Alice und Bob die Farbmischung ihres Gegenübers mit ihrer eigenen geheimen Farbe. Daraus entsteht wiederum eine neue Farbe (Ockerbraun), die für beide Kommunikationspartner gleich ist (Gelb + Orange + Türkis = Gelb + Türkis + Orange = Ockerbraun). Somit haben Alice und Bob eine gemeinsame geheime Farbe. Einer Drittperson ist es nicht möglich, die geheimen Farben von Alice und Bob herauszufinden, da diese Alices und Bobs geheime Farbzutaten nicht kennt.

Obwohl das mathematische Verfahren sicher ist und eine Brute-Force-Attacke an der Verarbeitungsgeschwindigkeit heutiger Prozessoren scheitert, bleiben zwei Gefahren:

* Unsichere, leicht erratbare Passwörter
* Der unbewusste Einsatz von gefälschten Public-Keys. Damit ist ein Public-Key gemeint, der vorgibt, der Person Bob zu gehören, tatsächlich aber von Mallory kreiert wurde. Die vermeintlich an Bob verschlüsselt verschickte Datei kann nun von Mallory problemlos geöffnet werden. Abhilfe schafft hier eine möglichst zentrale und vetrauenswürdige Public-Key-Verwaltung (PKI) oder ein Vetrauensnetzwerk.


## Hybride Verfahren
Symmetrische Verschlüsselungsverfahren haben das bereits besprochene Problem des Schlüsseltauschs und Schlüsselmanagements, das bei den asymmetrischen Verfahren so nicht besteht. Allerdings benötigen symmetrische Verfahren weniger Rechenzeit zur Erstellung des Chiffretexts als rein asymmetrische Verfahren. Darum liegt es auf der Hand, dass in einem hybriden Verfahren die Vorteile der beiden Verfahren genutzt werden:

* Asymmetrisches oder Public-Key-Verfahren für Schlüsselmanagement, zB. RSA
* Symmetrisches Verfahren zum Versenden der eigentlichen Nachricht, zB. RC4, DES, AES


## Aufgaben zur asymmetrischen Verschlüsselung

1. Spielen sie in Cryptool1 einen Schlüsseltausch gemäss Diffie-Hellman durch. Experimentieren sie mit verschiedenen, auch eigenen Parametern. (Sie finden das Tool unter Einzelverfahren&rarr;Protokolle&rarr;Diffie-Hellman-Demo...)
2. RSA-Verschlüsselung:<br>
Erzeugen sie zwei asymmetrische Schlüsselpaare: Eines für «Muster Felix» und eines für «Hasler Harry» (Sie finden das Tool unter Digitale Signaturen/PKI&rarr;PKI&rarr;Schlüssel erzeugen/importieren...)<br>
Verschlüsseln Sie nun eine Nachricht für Muster Felix und versuchen sie danach, den Text als Hasler Harry, danach als Muster Felix zu entschlüsseln. Was stellen sie fest? (Sie finden die Tools unter Ver-/Entschlüsseln&rarr;Asymmetrisch&rarr;RSA-Ver/Entschlüsselung...)
3. Im Gegensatz zum Diffie-Hellman-Verfahren (für Schlüsseltausch) kann RSA einen kompletten Text verschlüsseln. Sehen sie sich dazu  die RSA-Demo an. (Sie finden das Tool unter Ver-/Entschlüsseln&rarr;Asymmetrisch&rarr;RSA-Demo...)
4. Moderne Verschlüsselungsverfahren arbeiten hybrid. Schauen sie sich dazu die beiden Demos zu RSA-AES an. (Sie finden die Tools unter Ver-/Entschlüsseln&rarr;Hybrid&rarr;RSA-AES-Ver/Entschlüsselung...)
5. (Optionale Aufgabe für Mathe-Fans) Wir verwenden hier nochmals die RSA-Demo. (Sie finden das Tool unter Ver-/Entschlüsseln&rarr;Asymmetrisch&rarr;RSA-Demo...) Wir möchten der Sicherheit etwas auf den Zahn fühlen:<br>
Um den geheimen und öffentlichen Schlüssel zu erstellen, müssen zuerst zwei Primzahlen gewählt werden. Unter Primzahlen generieren kann bei Primzahl p bzw. q eine Primzahlunter- und obergrenze bestimmt werden, in dessen Bereich Primzahlen generiert werden. Öffentlich ist dann das RSA-Modul N und der öffentliche Schlüssel e. Wir möchten nun prüfen, wie gross die beiden Primzahlen p und q gewählt werden müssen, damit die Faktorisierung des RSA-Moduls N und damit das Knacken des geheimen Schlüssels d nicht so mühelos gelingen kann. Das RSA-Modul N kann mit folgendem Tool faktorisiert werden: Analyse&rarr;Asymmetrische Verfahren&rarr;Faktorisieren einer Zahl...<br>
Erstellen sie nun mit RSA-Demo bei kleinen, max. 2-stelligen Primzahlen das RSA-Modul N und lassen sie danach die Zahl im anderen Tool faktorisieren. Sie werden feststellen, dass die Faktorisierung in wenigen Augenblicken erledigt ist und somit der Geheimtext entschlüsselt werden könnte. Wiederholen sie nun den Versuch mit grossen Primzahlen. Sie können dazu im Menü «Primzahlen generieren» die Primzahlobergrenze p und q auf zum Beispiel 128 Bit (2^128) erhöhen. Versuchen sie nun das erhaltene RSA-Modul N mit dem Analyse-Tool zu faktorisieren. Sei werden sehen, dass dies nun nicht mehr so ohne weiteres (Zeitaufwand) gelingen wird.


