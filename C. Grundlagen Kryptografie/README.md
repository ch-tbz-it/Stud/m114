# C. Grundlagen Verschlüsselungsverfahren (Kryptografie)
Für dieses Kapitel stehen an der Schule insgesamt 6 Lektionen zu je 45 Minuten zur Verfügung.

Mit **Kryptografie** war ursprünglich die Wissenschaft der Verschlüsselung gemeint. Heute umfasst sie allgemein die  Informationssicherheit und die Widerstandsfähigkeit gegen Manipulation und unbefugtes Lesen.

* **Kryptologie**: Wissenschaft vom Entwurf, der Anwendung und der Analyse von kryptografischen Verfahren
* **Kryptografie**: Wie kann eine Nachricht ver- und entschlüsselt werden?
* **Kryptoanalyse**: Wie sicher ist ein Verschlüsselungsverfahren?


## Die Akteure bei einer verschlüsselten Datenübertragung:
![AliceAndBob](/GITressourcen/Daten_verschlüsseln/mallory.jpg)

## Was bedeutet Geheimhaltung?
![Motivation](/GITressourcen/Daten_verschlüsseln/intro.jpg)

## Verschlüsselungskonzepte Symmetrisch und Asymmetrisch

* **C.1 Symmetrische Verschlüsselung:** Symmetrische Verschlüsselungsverfahren zeichnen sich dadurch aus, dass der Absender die Botschaft mit demselben Schlüssel verschlüsselt, wie der Empfänger, der die Botschaft wieder entschlüsslet. Man unterscheidet zwischen historischen und heutzutags unsicheren Verfahren wie ROT etc. und aktuellen sicheren Verfahren wie AES.<br>
Dauer: 2 Lektionen<br>
[Hier geht's zum Unterrichtsmaterial](/C. Grundlagen Kryptografie/C.1 Symmetrische Verschlüsselung)
* **C.2 Asymmetrische Verschlüsselung:** Asymmetrische Verschlüsselungsverfahren zeichnen sich dadurch aus, dass jeder Teilnehmer ein Schlüsselpaar Public/Privat-Key besitzt. Dies erleichtert den Schlüsseltausch und verringert die Schlüsselanzahl.<br> 
Dauer: 3 Lektionen<br>
[Hier geht's zum Unterrichtsmaterial](/C. Grundlagen Kryptografie/C.2 Asymmetrische Verschlüsselung)

* **C.3 Digital signieren** 
Hier geht es nicht darum, einen Inhalt vor fremden Personen zu verbergen, sondern Authentizität, Integrität und Verbindlichkeit einer Nachricht zu gewährleisten. Es kommt dabei dieselbe Technik zur Anwednung, wie bei der asymmetrischen Verschlüsselung.<br>
Dauer: 1 Lektion<br>
[Hier geht's zum Unterrichtsmaterial](/C. Grundlagen Kryptografie/C.3 Digital signieren)

