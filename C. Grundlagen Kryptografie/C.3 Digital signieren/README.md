# C.3 Digital signieren
*Dauer: 1 Lektion<br>
Autor: ARJ<br>
Letzte Änderung: 7.10.2022*

## Die Motivation
Nicht immer ist es das Ziel, eine Nachricht zu verschlüsseln. Oftmals besteht auch das Bedürfnis, die Authentizität, Integrität, Verbindlichkeit etc. einer Nachricht zu erfahren, wie z.B:

* wer in meinem E-Shop bestellt hat
* wer mir diese oder jene E-Mail zugestellt hat
* ob wirklich der Absender das geschrieben hat, was ich da nun lese
* woher das Applet stammt, das ich gerade auf meinen PC lade
* ob es sich bei dem Update wirklich um das richtige und unmanipulierte Original handelt
* wohin meine Kreditkartennummer übermittelt wird
* wer bei einer Wahlveranstaltung gerade seine Stimme abgegeben hat
* ob der Inhalt von www.admin.ch wirklich von unserer Regierung stammt

Ausgehend von seinem Grundprinzip ist das Internet nicht sicher:

* Mitlesen von Daten (Sniffing)
* Vortäuschen falscher Identitäten (Spoofing)
* Angriffe auf die Verfügbarkeit (Denial-of-Service)
* Übertragen von Programmen mit Schadfunktion (Viren, Würmer...)
* Menschliches Fehlverhalten (Preisgabe von geheimen Daten)

Das möchte man erreichen:

* Authentisierung - Sicherstellung der Identität eines Kommunikationspartners
* Vertraulichkeit - Zugänglichkeit der Nachricht nur für bestimmten Empfängerkreis
* Integrität - Schutz vor Verfälschung von Nachrichten bei der Übermittlung
* Autorisierung - Prüfung der Zugriffsberechtigung auf Ressourcen
* Verfügbarkeit - Schutz vor Datenverlust, Sicherstellung des laufenden Betriebs
* Verbindlichkeit - Sicherer Nachweis der Absendung bzw. des Empfangs

Und da kommt die digitale Signatur ins Spiel:

* Sichere Identifizierung des Absenders eines Dokumentes
* Sicherheit vor nachträglichen Manipulationen des Dokumentes
* Elektronisch signierte Dokumente sind mit unterschriebenen Papierdokumenten gleich gesetzt
* Ist ein Prüfwert einer Information
* Die Digitale Signatur hat die Aufgabe einer Unterschrift und eines Siegels

## Das Konzept
Technisch kommt dasselbe Verfahren zur Anwendung, wie bei der asymmetrischen Verschlüsselung. Bis auf einen kleinen, aber nicht unwesentlichen Unterschied, nämlich der Verwendung der Schlüssel:

* Der Inhalt der Nachricht wird auf eine eindeutige Kenngrösse abgebildet. Dazu bedient man sich eines **Hash-Algorithmus**
* Der Hash-Wert wird mit dem **privaten Schlüssel** verschlüsselt und zur Nachricht hinzugefügt
* Der Empfänger kann mit Hilfe des **öffentlichen Schlüssel** des Senders prüfen, ob die Information wirklich vom Absender stammt und nicht verändert wurde

![Signieren](/GITressourcen/Daten_verschlüsseln/Signieren.jpg)

## Exkurs: Der Hash-Algorithmus

* Der Hashwert ist eine Art Fingerabruck eines Dokuments.
* Der Hashwert bildet einen beliebig langen Nachrichtentext auf einen Wert vorgegebener, kurzer Länge an (Hashwert, Prüfsumme)
* Aus dem Hashwert kann die ursprüngliche Nachricht nicht errechnet werden (Irreversibilität)
* Die Konstruktion von Nachrichten mit identischem Hashwert muss praktisch unmöglich sein
* Die zufällige Übereinstimmung von Hashwerten beliebiger Nachrichten ist sehr unwahrscheinlich (Kollisionsresistenz, Integrität prüfbar)

## Aufgabe Hashwert
Führen Sie nun im Cryptool die Hash-Demo aus. Sie finden diese unter Einzelverfahren &rarr; Hashverfahren &rarr; Hash-Demo...

## Aufgabe Dokument signieren
Erstellen sie ein kurzes Dokument und signieren sie dieses.<br>
Siehe Digitale Signaturen/PKI &rarr; Dokument signieren nzw. Dokument überprüfen.<br>
Nehmen sie am signierten Dokument eine kleine Änderung vor und überprüfen sie die Signatur erneut. Was stellen sie fest?

## Hashwert-Manipulation bei der digitalen Signatur
Der Nachricht muss ein eindeutiger Hashwert entsprechen. Mit unsicheren oder veralteten Hashverfahren ist dies aber nicht immer der Fall. Wie Sie in der folgenden Analyse der Hashverfahren erfahren dürfen, kann je nach gewähltem Hashverfahren eine zumindest teilweise Hashwert-Übereinstimmung von verschiedenen Nachrichten erreicht werden. Probieren Sie es doch einfach einmal mit Cryptool selber aus. Siehe Analyse &rarr; Hashverfahren &rarr; Angriff auf den Hashwert der digitalen Signatur...

Die einzelnen Schritte:

1. Erstellen sie eine Datei original.txt mit dem Textinhalt: «Verkaufe mein Notebook zu CHF 1500.-»
2. Erstellen sie von der soeben erstellten Datei original.txt eine Kopie mit dem Dateinamen backup.txt.
3. Erstellen sie eine Plagiats-Datei fake.txt mit dem Textinhalt: «Verkaufe mein Notebook zu CHF 150 .-» (Es fehlt absichtlich die letzte Null!)
4. Erstellen sie zu Kontrollzwecken je einen MD2-Hashwert von allen drei Dateien.<br>
Einzelverfahren &rarr; Hashverfahren &rarr; MD2<br>
Sie stellen fest: original.txt und backup.txt haben denselben Hashwert, fake.txt einen anderen.<br>
backup.txt kann nun gelöscht werden. Diese Datei brauchen wir nicht mehr.
5. Wählen sie nun Analyse &rarr; Hashverfahren &rarr; Angriff auf den Hashwert der digitalen Signatur...
8. Als harmlose Datei wählen sie original.txt
9. Als gefährliche Datei wählen sie fake.txt
Wählen sie bei den Optionen den schwächsten Hashalgorithmus MD2 und eine signifikante Bitlänge von 16 (Bit).<br>
Nach der Ausführung erhalten sie zwei Varianten von ihren Ausgangsdateien:<br>
«original.txt» ergibt «Harmlose Nachricht: MD2, <92 14>»<br>
«fake.txt» ergibt «Gefährliche Nachricht: MD2, <92 14>»<br>
Das bedeutet: Cryptool hat von beiden Ausgangsdateien Varianten mit kleinen Ergänzungen/Änderungen gefunden bzw. erstellt, die sich in den ersten 16 Bit des Hashwerts nicht unterscheiden: <92 14>
6. Sie können nun diesen Vorgang mit einer längeren signifikanten Bitlänge wie z.B. 24,32, etc. wiederholen.<br>
Sie werden feststellen, dass der Suchvorgang in Cryptool immer länger dauert. Bei einer signifikanten Bitlänge von 128 wäre der Hashwert bei der Textdatei original.txt und fake.txt komplett berechnet. Das heisst, es liegen nun zwei Dokumente vor, die denselben Hashwert besitzen.

Was ist nun das Gefährliche dabei:<br>
Würden sie als Bösewicht nun eine Variante mit dem modifizierten aber sonst korrekten Text «Verkaufe mein Notebook zu CHF 1500.-» ihrem Opfer Felix Muster zur digitalen Signierung vorlegen und dieser auch tatsächlich unterschreiben, wäre ihre Schelmerei schon zur Hälfte gelungen: Sie besässen ein modifiziertes korrektes Dokument mit gültiger Signatur, würden dieses aber durch ihre gefährliche Datei mit dem modifizierten Fake-Text «Verkaufe mein Notebook zu CHF 150 .-» ersetzen. Der von Felix Muster signierte Hashwert gilt ja für beide modifizierten Dokumente. Würde nun Susi Sorglos das gefälschte Dokument inklusive Signatur erhalten, dessen Echtheit überprüfen und dann auch noch lesen, wäre der Schaden schon angerichtet: Die ahnungslose Frau würde annehmen, das Dokument stammt tatsächlich und unverfälscht von Felix Muster, was ja infolge ihrer Manipulation (Modifikationen) nicht zutrifft und würde vielleicht sogar auf den Kauf des für CHF 150.- angebotenen Notebooks bestehen.
Geht es nur um belanglose Dinge, stellt das kein grosses Problem dar. Handelt es sich aber um Votings, rechtlich verbindliche Offerten oder sogar Software-Updates, droht nachhaltiger Ärger.

Fazit:<br>
Niemals fremde Dokumente unbekannten Inhalts signieren!<br>
(Um jetzt aber dieser Erfahrung etwas Brisanz zu nehmen, soll gesagt sein, dass während der Berechnung aller Hash-Bits (in unserem Fall 128) doch etwas Zeit vergeht (Die ersten 64 Bit am PC berechnen dauert ca. 1..4 Tage - HW/SW-abhängig/Stand 2020) und das der MD2 ja auch schon etwas in die Jahre gekommen und bei aktuellen Signier-Tools schon längst durch leistungsfähigere und kaum manipulierbare Algorithmen ersetzt worden ist.)






