# D. Gesicherte Datenübertragung
Für dieses Kapitel stehen an der Schule insgesamt 6 Lektionen zu je 45 Minuten zur Verfügung.

* D.1 Public Key Infrastruktur<br>
Dauer: 1 Lektion<br>
[Hier geht's zum Unterrichtsmaterial](/D. Gesicherte Datenübertragung/D.1 Public Key Infrastruktur)
* D.2 Internet und Zertifikate<br>
Dauer: 1 Lektion<br>
[Hier geht's zum Unterrichtsmaterial](/D. Gesicherte Datenübertragung/D.2 Internet und Zertifikate)
* D.3 PGP und OpenPGP<br>
Dauer: 2 Lektionen<br>
[Hier geht's zum Unterrichtsmaterial](/D. Gesicherte Datenübertragung/D.3 PGP und OpenPGP)
* D.4 Sichere E-Mails<br>
Dauer: 2 Lektionen<br>
[Hier geht's zum Unterrichtsmaterial](/D. Gesicherte Datenübertragung/D.4 Sichere E-Mails)

