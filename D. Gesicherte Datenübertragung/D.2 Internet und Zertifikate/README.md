# D.2 Internet und Zertifikate
*Dauer: 3 Lektionen<br>
Autor: ARJ<br>
Letzte Änderung: 30.1.2023*

## Sichere Webseiten mit HTTPS → TLS + HTTP
Wie sicher ist eine Webseite? Vor allem dann, wenn sensitive Daten darüber verschickt werden, wie z.B. bei Webshops und Onlinebanking, wo Webformulareingaben wie Benutzernamen und Passwörter vor fremden Augen versteckt werden sollten. Gefürchtet ist auch die "Man-in-the-middle-Attacke, wo eine Person zwischen Webbrowser und Webserver Daten anzapft und modifiziert weiterleitet, bzw. die Antwort abfängt und in seinem Sinne "bereinigt" an den Kunden zurückgibt. Im weiteren ist es ein grosses Bedürfnis, sicher zu sein, dass herunterladbare Programme/Patches auch echt sind bzw. tatsächlich vom vermeintlichen Absender stammen. Abhilfe schafft da SSL/TLS, ein Verschlüsselungsprotokoll, dass zwischen Transport-Layer und Applikationslayer (siehe ISO-OSI-Schichtenmodell) für Sicherheit sorgt. Möchte man seine Webseite damit aufrüsten, stellt sich sofort wieder die Frage der Authentizität öffentlicher Schlüssel: d.h. es führt kein Weg an einem offiziellen SSL/TLS Zertifikat vorbei. Ausserdem muss der Webhoster SSL/TLS auch unterstützen.<br><br>

**Wie damalige Beiträge im Internet zeigen, werden ab dem Jahre 2018 verschlüsselte Webseiten fast zur Pflicht:**

* Ein SSL/TLS-Zertifikat macht deine Seite sicherer und ist gut für Google.
* Schon vor gut zwei Jahren hatte Google angekündigt verschlüsselte Seiten bei den Suchergebnissen bevorzugt zu behandeln.
* Der Google-Browser Chrome wird unverschlüsselte Seiten demnächst mit einem "Nicht sicher" in der Adresszeile brandmarken.
* Verschlüsselte Kommunikation sollte ein Grundrecht im Internet sein.
* Eine verschlüsselte Webseite ist eben nicht per se vertrauenswürdig.
* Allein mit dem Stichwort Paypal hätten seit Januar 2016 fast 15.000 eindeutig als Phishing-Seiten identifizierte Domains Zertifikate von Let's Encrypt erhalten.
* Etwas über zwei Jahre nach dem Start der gemeinnützigen Zertifizierungsstelle Let's Encrypt (2016) stellt diese nun über die Hälfte aller SSL-Zertifikate im Netz.

## SSL/TLS

* SSL = Secure Sockets Layer (Veraltet! Wurde durch TLS abgelöst)
* TLS = Transport Layer Security (Transportschichtsicherheit)

TLS liegt zwischen der Transportschicht (z.B. TCP via Port 443) und der Applikationsschicht (z.B. HTTP). Man nennt dieses Gespann dann HTTPS. HTTPS ist also kein eigenes Protokoll, sondern die Kombination von TLS mit HTTP. HTTPS ist im Webbrowser bereits integriert, muss also nicht extra nachinstalliert werden. Als Software zum Betrieb eines HTTPS-fähigen Webservers wird eine SSL-Bibliothek wie OpenSSL benötigt. Das digitale Zertifikat für SSL, das die Authentifizierung ermöglicht (Server und die Domain sind eindeutig), ist vom Server bereitzustellen und kann z.B. bei einer Zertifizierungsstelle erworben werden.
Der Client-Browser ruft eine sichere Webseite mit https, gefolgt von der URL auf. Ist eine HTTPS-Verbindung etabliert, wird in der Statusleiste des Webbrowsers ein Schloss- oder Schlüssel-Symbol eingeblendet oder die Adresszeile in ihrer Farbe geändert.

**So erfolgt ein Standard-SSL-Handshake, wenn ein RSA-Algorithmus zum Schlüsselaustausch verwendet wird:**
![SSLTLS](/GITressourcen/Daten_verschlüsseln/SSLTLS.jpg)

1. Client Hello: Informationen, die der Server benötigt, um mit dem Client mithilfe von SSL zu kommunizieren. Dies beinhaltet die SSL-Versionsnummer, Verschlüsselungseinstellungen und sitzungsspezifische Daten.
2. Server Hello: Informationen, die der Server benötigt, um mit dem Client mithilfe von SSL zu kommunizieren. Dies beinhaltet die SSL-Versionsnummer, Verschlüsselungseinstellungen und sitzungsspezifische Daten.
3. Authentifizierung und Pre-Master Secret: Der Client authentifiziert das Serverzertifikat. (z.B. Common Name/Datum/Aussteller). Der Client (abhängig von der Verschlüsselungssoftware) erstellt den Pre-Master-Secret-Wert für die Sitzung, verschlüsselt ihn mit dem öffentlichen Schlüssel des Servers und sendet den verschlüsselten Pre-Master-Secret-Wert an den Server.
4. Entschlüsselung und Master Secret: Der Server entschlüsselt den Pre-Master-Secret-Wert mithilfe seines privaten Schlüssels. Sowohl Server als auch Client führen Schritte aus, um den Master-Secret-Wert mithilfe des vereinbarten Verschlüsselungsverfahrens zu generieren.
5. Verschlüsselung mit Sitzungsschlüssel: Client und Server tauschen Meldungen aus, um mitzuteilen, dass zukünftige Nachrichten verschlüsselt werden.

## Die sichere Verbindung zum Mailserver
Was Sie beim Einrichten eines E-Mail-Clients beachten sollten: Beim Abfragen und Verschicken von E-Mail's sollte man auch an die Verbindungssicherheit denken: Unverschüsselt können Sie auf dem Transportweg (Mail-Client zu Mail-Server) leicht abgehört werden. Darum ist es von Vorteil, eine Verschlüsselungsmethode wie SSL/TLS einzusetzen, sofern dies von Ihrem Mail-Provider auch angeboten wird. Allerdings muss man wissen, dass es sich dabei "nur" um eine Transportverschlüsselung handelt. Das bedeutet, dass Ihre EMails auf dem Weg zum Mailserver nicht eingesehen werden können. Auf dem Mailserver liegen Ihre EMails allerdings unverschlüsselt vor, ausser Sie bedienen sich einer Mailverschlüsselung. OpenPGP und S-MIME sind dabei die beiden wichtigsten Standards für eine E-Mail-Verschlüsselung.

## SSH-Login
Secure Shell (SSH) ist eine Möglichkeit, sich sicher bei einem Remote-Computer anzumelden, da alle Daten zwischen dem lokalen Computer und dem Remote-Computer in beide Richtungen verschlüsselt werden. Dazu verwendet man asymmetrische SSH-Schlüssel: Auf dem lokalen Computer wird ein SSH-Schlüsselpaar generiert, der private Schlüssel verbleibt auf dem lokalen PC und der öffentliche Schlüssel wird auf den entfernten Computer hochgeladen.<br>
Die Arbeitsschritte (Lokaler LIN-PC=Linux-PC und Remote-WIN-PC):

* Auf dem LIN-PC existiert z.B. der Benutzer HansMuster<br>
(Falls ihr LIN-PC kein CH-Layout aufweist, dies entweder ändern oder bedenken, dass y und z vertauscht sind bzw. der Bindestrich sich mit der ?-Taste aufrufen lässt und der Slash / sich auf der Taste _ befindet)

* IP-Adresse des LIN-PCs prüfen und gegebenfalls anpassen:<br>
sudo opt install net-tools (NetTools installieren, sofern noch nicht geschehen.)<br>
ifconfig (Aktuelle IP-Adresse überprüfen)<br>
sudo ifconfig eth0 192.168.1.111 netmask 255.255.255.0 (IP gem. Vorgabe anpassen)<br>
ifconfig (Einstellung überprüfen)<br>

* Verbindung lokaler LIN-PC und Remote-WIN-PC mit ping überprüfen.

* Auf dem LIN-PC ssh installieren:<br>
sudo apt install openssh-server

* Auf dem LIN-PC ssh-Installation überprüfen:<br>
sudo systemctl status ssh (Sollte active (running) als Antwort enthalten)<br>
sudo systemctl enable ssh (Falls ssh noch nicht aktiv ist)<br>
sudo systemctl start ssh (Falls ssh noch nicht aktiv ist)

* Auf dem LIN-PC das .ssh-Verzeichnis im eigenen Homeverzeichnis auf bereits existierende SSH-Keys untersuchen:<br>
ls -la ~/.ssh/

* Auf dem LIN-PC neuer Schlüsselpaar erstellen:<br>
ssh-keygen -C mein_SSH-Keyname (mein_SSH-Keyname durch meinen bevorzugten Schlüsselnamen ersetzen)<br>
(Hinweis: Mit der Option -t kann man den zu erstellenden Schlüsseltyp angeben. Z.B. ssh-keygen -t ed25519 -C mein_SSH-Keyname. Mögliche Schlüsseltypen sind dsa, ecdsa, ecdsa-sk, ed25519, ed25519-sk, rsa. Weitere Optionen entnehmen man den LIN-Manual-Pages.)

* Jetzt sollte auf dem LIN-PC mindesten ein Schlüsselpaar existieren:<br>
ls -la ~/.ssh/ ergibt mein_SSH-Keyname als PrivateKey und mein_SSH-Keyname.pub als PublicKey.

* Den soeben erstellten öffentlichen Schlüssel auf den WIN-PC kopieren.

* Auf dem WIN-PC ein Terminal öffnen und Remote-Verbindung etablieren:<br>
ssh HansMuster@192.168.1.111 (HansMuster ist ein LIN-PC-User. Die IP-Adresse entsprechend anpassen)<br>
ssh HansMuster@192.168.1.111 -p Port-Nummer (Falls nicht Standardport 22 verwendet wird)<br>
Anstatt der IP-Adresse kann auch ein Domänenname verwendet werden, sofern dieser über z.B. DNS aufgelöst wird.<br>
Nun erscheint das Linux-Shell-Prompt des LIN-PCs.



## TLS-Zertifikate
Zertifikate ermöglichen es zusammen mit der Public Key Infrastruktur (PKI), Informationen im Internet sicher und verschlüsselt zu übertragen.<br>
Ein digitales Zertifikat bestätigt bestimmte Eigenschaften von Personen oder Objekten. Dessen Authentizität und Integrität kann durch kryptografische Verfahren geprüft werden.<br><br>

**Folgende TLS Zertifikatsarten werden unterschieden:**

* Domain Validated
* Organization Validated
* Extended Validation

**Zertifizierungsstellen: (Auswahl)**

* Letsencrypt: Bietet seit 2015 kostenlose TLS-Zertifikate an.<br>
So lange ein Domain validiertes Zertifikat ausreicht ist Let’s Encrypt auf jede Fall zu empfehlen. Soll aber ein organisationsvalidiertes oder erweitert validiertes Zertifikat genutzt werden, um zum Beispiel die Existenz der jeweiligen Firma ersichtlich zu machen, muss auf einen anderen Anbieter zurückgegriffen werden.<br>
[Link zum Anbieter.](https://letsencrypt.org/de)
* CaCert: Ist eine der älteren gemeinschaftsbetriebenen, nichtkommerziellen Zertifizierungsstellen. CAcert stellt für jedermann kostenfrei X.509-Zertifikate für verschiedene Einsatzzwecke aus und soll eine Alternative zu den kommerziellen Zertifizierungsstellen sein, die zum Teil recht hohe Gebühren für ihre Zertifikate erheben.<br>
Der Webbrowsersupport ist limitiert, was einer Verbreitung von CaCert bisher eher hinderlich war.
[Link zum Anbieter.](http://www.cacert.org)
* Verysign: Ist ein kommerzieller Anbieter aus den USA. 
[Link zum Anbieter.](https://www.verisign.com)
* Globalsign: Ist ein kommerzieller Anbieter aus Japan. 
[Link zum Anbieter.](https://www.globalsign.com)
* DigiCert/Quovadisglobal: Ist ein kommerzieller Anbieter aus der Schweiz. 
[Link zum Anbieter.](https://www.quovadisglobal.ch)
* Swisssign: Ist ein kommerzieller Anbieter aus der Schweiz. 
[Link zum Anbieter.](https://www.swisssign.com)

## Aufgaben und Kontrollfragen

1. Erstellen sie eine virtuelle Linux-Maschine mit z.B. VirtualBox und Ubuntu. Richten sie nun auf ihrem WIN-PC eine Remoteverbindung via ssh zu ihrem Linux-PC ein. Überprüfen sie die Verbindung. Wäre auch eine graphische Anbindung möglich?
2. In dieser Übung untersuchen wir eine http-Verbindung und eine https-Verbindung mit dem Network-Sniffer Wireshark.<br>
[https://www.wireshark.org/](https://www.wireshark.org/)<br>
[http://www.example.ch](http://www.example.com)<br>
[https://www.zkb.ch](https://www.zkb.ch)<br>
Untersuchen sie speziell die OSI-Layer 2,3,4 und 7. Was stellen sie fest? Wo liegen die Unterschiede zwischen http und https? Zusatzfrage: Kann man mit Wireshark bei einer https-Verbindung trotzdem herausfinden, welche Webseite besucht wurde?
3. Öffnen sie die beiden folgenden Webseiten und achten sie auf die Unterschiede in der Webadresszeile. Was stellen sie bezüglich Protokoll und Zertifikat fest?<br>
[https://juergarnold.ch](https://juergarnold.ch)<br>
[https://www.zkb.ch](https://www.zkb.ch)<br>
4. Wenn sie sich mit Zertifikaten befassen, fallen ihnen früher oder später folgende Anbieter bzw. Webseiten auf:<br>
[http://www.cacert.org](http://www.cacert.org)<br>
[https://letsencrypt.org/de](https://letsencrypt.org/de)<br>
Was genau wird hier zu welchen Konditionen angeboten?
5. Folgende TLS Zertifikatsarten werden unterschieden:<br>
Domain Validated<br>
Organization Validated<br>
Extended Validation<br>
Sie möchten einen Webshop betreiben, wo mit Kreditkarte bezahlt werden kann. Welcher Zertifikatstyp ist der richtige?
6. Studieren sie den Beitrag auf der Webseite Let's Encrypt "Wie es funktioniert"<br>
[https://letsencrypt.org/de/how-it-works/](https://letsencrypt.org/de/how-it-works/)
7. Was ist der Unterschied zwischen OpenPGP und X.509?
8. Erklären sie den Aufruf einer sicheren Webseite. (HTTPS)
9. Wie ist der Ablauf beim Protokoll TLS? Wo genau kommen die Zertifikate ins Spiel?
10. Was bedeutet S/MIME?
11. Aus gesetzlichen Gründen sind sie verpflichtet, den gesamten geschäftlichen EMail-Verkehr zu archivieren, auch den verschlüsselten. Was ist das Problem dabei und wie könnte man dies lösen?
12. Zusatzaufgabe für Lernturbos: Versuchen sie mit Wireshark einen Standard-TLS-Handshake zu dokumentieren.
