# D.1 Public Key Infrastruktur
*Dauer: 1 Lektion<br>
Autor: ARJ<br>
Letzte Änderung: 30.1.2023*

## PKI
So sicher asymmetrische Verschlüsselungsverfahren auch sein mögen, wirklich geschützt ist man nur, wenn man dem ausgewählten Public-Key auch vertrauen kann. Das heisst, dass z.B. der Public-Key von Bob auch wirklich Bob gehört und nicht ein mit Bob umbeschrifteter Public-Key von Mallory ist. Um dies sicherzustellen, muss entweder eine Art Vetrauensnetz aufgebaut werden, oder die Public/Private-Keys werden von offiziellen Stellen, ähnlich den Passbüros, ausgestellt. Diese Zertifizierungsstellen klären vor der Schlüsselausgabe die Indentität des Antragsteller (mehr oder weniger) seriös ab und garantieren danach (mehr oder weniger) für die Echtheit dieser Schlüssel, wenn jemand diese überprüft haben möchte.<br><br>

Mit Public-Key-Infrastruktur (PKI) bezeichnet man in der Kryptologie ein System, das digitale Zertifikate ausstellen, verteilen und prüfen kann. Die innerhalb einer PKI ausgestellten Zertifikate werden zur Absicherung rechnergestützter Kommunikation verwendet.

**Die Bestandteile einer PKI:**

* Digitale Zertifikate
* Zertifizierungsstelle (Certificate Authority, CA)
* Registrierungsstelle (Registration Authority, RA)
* Zertifikatsperrliste (Certificate Revocation List, CRL)
* Verzeichnisdienst (Directory Service)
* Validierungsdienst (Validation Authority, VA)
* Dokumentationen: CP (Certificate Policy), CPS (Certification Practice Statement), PDS (Policy Disclosure Statement)
* Subscriber: Inhaber von Zertifikaten
* Participant: Nutzer von Zertifikaten

**Der ITU-T-Standard X.509:**
Die Alternative zur dezentralen Schlüsselverwaltung (Web-of-Trust), wie wir sie bei OpenPGP vorfinden, ist eine zentrale, hierarchische Verwaltung. X.509 ist dabei ein ITU-T-Standard für eine Public-Key-Infrastruktur zum Erstellen digitaler Zertifikate.<br>
Das X.509-Zertifikat beweist, dass desen Besitzer seine Identität bzw. Glaubwürdigkeit von einer CA (=Certification Authority) bescheinigen hat lassen. Das heisst: Personenangaben werden mit dem Schlüsselpaar gekoppelt (Elektronischer Ausweis), ausgestellt von einer vertrauenswürdigen Instanz TC (=Trust Center) oder CA. Diese Schlüssel unterscheiden sich von denen bei OpenPGP!

**Die Aufgaben von TC’s bzw. CA's (X.509):**

* Zertifikate ausstellen - Korrekte Identifikation des Teilnehmers, Zertifizieren von User, Server, Sub CA's. Erstellung des Zertifikates durch digitale Signatur über Identifikationsdaten  und öffentlichen Schlüssel eines Teilnehmers.  Bei Ausgabe eines Zertifikates an einen Teilnehmer ist dieser über Funktionalität  und Gefahren von zertifizierten Signaturschlüsseln zu unterrichten.
* Veröffentlichen der Public-Keys der User und Publizieren eigenes Zertifikat
* Verwalten bzw. Archivierung angelaufener Zertifikate
Zertifikate zurückziehen oder für ungültig erklären
Festlegung der Gültigkeitsdauer von Zertifikaten
*Zeitstempeldienst - Absicherung im Falle des Diebstahls des geheimen Schlüssels. Nachweis einer zeitpunktsbezogenen Willensbekundung. Nachweis und Sicherheit, dass die digitale Signatur nicht nach Ablauf der Gültigkeit des Zertifikates erstellt wurde.

**GPG4WIN unterstützt neben persönlichen OpenPGP-Schlüsselpaaren (Dezentral, Web-of-Trust) auch X.509-Schlüsselpaare (Zentrale Zertifizierungsinstanz, CA). Die beiden Schlüsselvarianten sind untereinander aber nicht kompatibel!**

## Kontrollfragen zum Thema

1. Wie kann ich den Public-Key verifizieren?
2. Was versteht man unter Public Key Infrastruktur?
3. Was bedeutet Certification-Authority (CA) und was Trust-Center (TC)?
4. Finden sie heraus, wer das Zertifikat für die Bankwebseite www.ubs.com ausgestellt hat und wie lange es gültig ist.
5. Wiederholen sie das ganze für die Schulwebseite www.tbz.ch
6. Und zulezt noch für die Webseite www.example.com
7. Wählen sie irgendeine Applikation aus, die auf ihrem PC installiert ist. Stellen sie sich nun vor, sie müssten diese von Hand aktualisieren oder aus Kompatibilitätsgründen auf eine frühere Version zurückstufen. Wo finden sie aktuelle und frühere Versionen ihrer Software und wie wird sichergestellt, dass die dort angebotene SW-Version auch wirklich echt ist bzw. vom SW-Entwickler stammt?

