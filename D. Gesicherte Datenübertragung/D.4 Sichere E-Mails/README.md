# D.4 Sichere E-Mails
*Dauer: 2 Lektionen<br>
Autor: ARJ<br>
Letzte Änderung: 14.8.2022*

## Sichere E-Mails mit S/MIME
S/MIME = Secure Multipurpose Internet Mail Extensions<br>
Dies ist ein Standard für die Verschlüsselung und das Signieren von MIME-gekapselter E-Mail durch ein hybrides Kryptosystem (Asymmetrisch/Symmetrisch). S/MIME wird von den meisten modernen Mailclients unterstützt. Es erfordert X.509-basierte Zertifikate für den Betrieb.<br>
Schlüsselvarianten: S/MIME ist die Alternative zu OpenPGP!

**S/MIME-Zertifikate:**

* X.509 ist ein Standard für eine Public-Key-Infrastruktur (PKI) zum Erstellen digitaler Zertifikate.
* Mit Public-Key-Infrastruktur (PKI) bezeichnet man in der Kryptologie ein System, das digitale Zertifikate ausstellen, verteilen und prüfen kann. Die innerhalb einer PKI ausgestellten Zertifikate werden zur Absicherung rechnergestützter Kommunikation verwendet.
* Die Anbieter von kostenpflichtigen S/MIME-Zertifikaten für sichere E-Mail-Kommunikation klassifizieren diese meist in drei Klassen. Dabei sichert bei Klasse 1 die Zertifizierungsstelle (CA) die Echtheit der E-Mail-Adresse zu und nur diese ist Teil des Zertifikats. Bei Klasse 2 wird zusätzlich der zur E-Mail-Adresse gehörende Name in das Zertifikat mit aufgenommen, sowie die Organisation/Firma. Diese Daten werden mithilfe von Drittdatenbanken und Ausweiskopien verifiziert. Bei Zertifikaten der Klasse 3 muss der Antragsteller sich persönlich ausweisen.
* Manche Unternehmen und nichtkommerzielle Organisationen bieten kostenlose S/MIME-Zertifikate an. Es können dabei nach einer Registrierung mehrere Zertifikate erstellt werden, die aber erst nach einer gewissen Anzahl von Identitätsnachweisen den Namen beinhalten. Diese können durch Mitglieder in einem Web of Trust oder anderen vertrauenswürdigen Stellen wie Rechtsanwälten oder Wirtschaftstreuhändern erfolgen. Grundsätzlich zerstört eine Ausstellung von Zertifikaten durch Anbieter ohne Identitätsüberprüfung des Antragstellers den Grundgedanken des sicheren Informationsaustauschs zwischen zwei Computern im Netz. So ist es bei einigen Zertifikatsausstellern tatsächlich möglich, mit erfundenen Betreiberangaben ein Zertifikat für eine völlig fremde Website zu erhalten. Der Benutzer würde über ein Umleiten der Informationsübertragung beispielsweise durch den Browser nicht mehr informiert, wenn die Zertifizierungsstelle durch den Browserhersteller von vornherein als vertrauenswürdig eingestuft wurde.

## E-Mails im dem Mailclient Thunderbird verschlüsseln
Mozilla's Thunderbird ist OpenSource und neben Microsoft Outlook ein sehr häufig eingesetzter Mail-Client zum Lesen und Schreiben von News und E-Mails.<br><br>

**Wie den Mailclient Thunderbird einrichten?**<br>
Die Zugangsdaten zu dem persönlichen E-Mail-Account wie wie E-Mail-Adresse, Login-Name, Passwort, Mail-Eingangs/Ausgangserver liegen bereit? Dann kann es losgehen:

1. Thunderbird herunterladen. Hier findet man Thunderbird: www.thunderbird.net
2. Thunderbird-Einrichtung starten und eigenen EMail-Account einrichten.
3. Nachdem man Name und EMail-Adresse eingegeben hat, kann man mit «Manuell einrichten» die Mailserver-Werte direkt eingeben.
4. Dazu wählt man: IMAP (Nachrichten auf dem Server speichern)
5. Nun die Kontoerfassung abschliessen. Wenn Thunderbird den EMail-Account nicht erfolgreich prüfen kann, wurden falsche Angaben gemacht. (Stimmt der Username, Passwort, Eingangs-/Ausgangsserver, Port-Nr. etc.?)

**Wie den Mailclient Thunderbird einsetzen?**<br>
Bevor Thunderbird für die EMail-Verschlüsselung bzw. Signierung eingesetzt werden kann, müssen noch ein paar Konfigurationen erledigt werden. Wie bei PGP4WIN/Kleopatra auch, stehen hier beide Schlüsselvarianten OpenPGP und S/MIME-X.509 zur Verfügung. Wir beschränken uns wiederum auf OpenPGP-Schlüssel.

1. In der oberen Menüzeile rechts aussen (Drei waagrechte Striche übereinander) → Anwendungsmenü von Thunderbird anzeigen
2. Extras → OpenPGP Schlüssel verwalten: Hier können sie ihr eigens Schlüsselpaar oder PublicKey ihrer Kommunikationspartner importieren.
Datei → Öffentliche(n) Schlüssel aus Datei importieren
Datei → Geheime(n) Schlüssel aus Datei importieren
Die Schlüssel können z.B. vorher aus Kleopatra exportiert werden.
Sie können unter «Erzeugen» aber auch ein neues Schlüsselpaar erstellen.
3. Nun müssen sie überprüfen, ob ihrem EMail-Account bereits ein Schlüssel zugewiesen wurde: Anwendungsmenü von Thunderbird → Konten-Einstellungen → Ende-zu-Ende-Verschlüsselung: Hier sollte unter OpenPGP angezeigt werden: Thunderbird verfügt über 1 persönlichen OpenPGP-Schlüssel für ...
Dies funktioniert aber nur, wenn sich ihr persönlicher, in Thunderbird importierter Schlüssel auch auf ihre EMail-Adresse bezieht. Im Zweifelsfall generieren sie ein neues, persönliches Schlüsselpaar in Thunderbird, dass sie nun ihrem EMail-Account zuweisen können.
4. Achten sie darauf, dass der Schlüssel auch tatsächlich verwendet wird. Es darf nicht die Option «Keiner - OpenPGP für diese Identität nicht verwenden» selektiert sein, sondern der Schlüssel darunter!
5. In diesem Menü lässt sich auch einstellen, ob standardmässig EMails verschlüsselt und/oder signiert werden sollen. Darauf verzichten wir vorerst einmal.

Falls man bereits PublicKeys von Klassenkameraden imporiert hat, kann man nun mit dem Verschlüsseln und Signieren beginnen:

1. Unter Verfassen die EMail-Adresse des Empfängers eingeben.
2. Prüfen sie unter Sicherheit/Verschlüsselungstechnologie ob auch OpenPGP aktiv ist.
3. Da sie EMails nicht automatisch verschlüsseln, aktivieren sie nun dies für die aktuelle EMail.<br>
Zur Auswahl stehen:<br>
Sicheheit/Nur mit Verschlüsselung senden<br>
Sicheheit/Nachricht unterschreiben<br>
Meinen öffentlichen Schlüssel anhängen<br>
5. Zur Kontrolle: Unten links erscheint OpenPGP und ein entsprechendes Icon.

## Auftrag Mailclient Thunderbird

**Als Vorarbeit sollen sie einen Test E-Mail-Account anlegen:**<br>
Beim Austesten der E-Mail-Verschlüsselung besteht die Gefahr, aus Unachtsamkeit seinen offiziellen E-Mail-Account zu schädigen. (Mails versehentlich vom Mailserver löschen etc.). Darum empfiehlt es sich, einen Test-E-Mail-Account bei einem Provider ihrer Wahl oder z.B. bei Swisscom einzurichten.
(Einige Provider verlangen beim Eröffnen eines neuen E-Mail-Accounts die Überprüfung ihrer Identität über eine Mobilenummer, Festnetznummer oder auf dem Postweg. Bei Swisscom können sie zurzeit (Stand Feb. 2023) unter Bluewin E-Mail light ohne Swisscom Internet-Abo mit einem zuvor eingerichteten Swisscom-Login kostenlos einen EMail-Account einrichten.)<br><br>

**Beispiel: Bei Swisscom würden die Zugangsdaten wie folgt lauten:**

* Name: Ihr Vorname und Nachname
* E-Mail-Adresse: xyz@bluewin.ch (Was halt noch so frei ist ;-)
* Passwort: ... (Achtung: Nicht das Swisscom-Portal-Passwort sondern das Swisscom-Mail-Passwort verwenden!)
* SSL/TLS: Jeweils aktiviert.
* Posteingangsserver:<br>
IMAP4: imaps.bluewin.ch (Port 993)<br>
POP3: pop3s.bluewin.ch (Port 995)
* Postausgangsserver:<br>
SMTP: smtpauths.bluewin.ch (Port 465)

Überprüfen Sie Ihren neuen E-Mail-Account, indem Sie Sich gegenseitig E-Mails zuschicken. Sie können dazu z.B. Swisscom-Webmail benutzen. Swisscom bietet auf seiner Webseite übrigens entsprechende Hilfestellung. Z.B. auch beim Einrichten Ihres Kontos in Outlook, Thunderbird, auf Tablets, Smartphones etc.<br><br>

**Thunderbird auf ihrem PC/Notebook installieren:**<br>
Installieren Sie auf Ihrem Notebook den E-Mail-Client Mozilla Thunderbird und richten sie ihr E-Mail-Konto darin ein. 
[Thunderbird können sie hier herunterladen.](https://www.thunderbird.net) Bei der Verschlüsselung fokussieren wir uns auf OpenPGP-Schlüssel. Die Alternative wären S/MIME-Zertifikate.<br><br>

**Thunderbird verwenden:**<br>

1. Suchen sie sich im Klassenverband einen EMail-Partner aus, mit dem sie EMails austauschen möchten.
2. Importieren sie den Public-Key dieser Person.
3. Erstellen und verschlüsseln sie eine EMail an ihr Gegenüber.
4. Erstellen und signieren sie eine EMail an ihr Gegenüber.
5. Erstellen, verschlüsseln und signieren sie eine EMail an ihr Gegenüber.
6. Prüfen sie die verschlüsselten und/oder signierten EMails ihres Partners.

