# D.3 PGP und OpenPGP
*Dauer: 2 Lektionen<br>
Autor: ARJ<br>
Letzte Änderung: 14.8.2022*

## Pretty Good Privacy: PGP und OpenPGP
PGP ist ein von Phil Zimmermann entwickeltes Programm zur Verschlüsselung und Signieren von Daten und arbeitet auf hybrider Basis. Es benutzt ein PrivateKey/PublicKey-Verfahren wie bei RSA. PGP basiert dabei auf dem sogenannten Web of Trust, bei dem es keine zentrale Zertifizierungsstelle gibt, sondern ein Vertrauensnetzwerk, das von den Benutzern selbst verwaltet wird. Mit PGP kann man wahlweise eine Nachricht signieren, verschlüsseln oder signieren und verschlüsseln.<br><br>

Um von IT-Multis unabhängig zu bleiben, wurde 1998 der OpenPGP-Standard entwickelt. Das unter der GNU-GPL stehende Programm GnuPG bzw. GPG4WIN ist wiederum eine Implementierung von OpenPGP. GPG4WIN unterstützt neben persönlichen OpenPGP-Schlüsselpaaren (Dezentral, Web-of-Trust) auch X.509-Schlüsselpaare (Zentrale Zertifizierungsinstanz, CA). Die beiden Schlüsselvarianten sind untereinander aber nicht kompatibel!<br><br>

OpenPGP: Sowohl Private/PublicKey als auch verrschlüsselte Nachricht sind ASCII-Dateien (.asc)<br><br>

**Beispiel eines öffentlichen OpenPGP-Schlüssels (Auszug):**

    -----BEGIN PGP PUBLIC KEY BLOCK----- 
    Version: GnuPG v2 
    mHjzBFYjc. 
    ... 
    -----END PGP PUBLIC KEY BLOCK-----
	  
**Beispiel eines privaten OpenPGP-Schlüssels (Auszug):**

    -----BEGIN PGP PRIVATE KEY BLOCK-----
    Version: : GnuPG v2
    wqdsf
    ...
    -----END PGP PRIVATE KEY BLOCK-----
	
**Beispiel einer OpenPGP-verschlüsselten Nachricht (Auszug):**

    -----BEGIN PGP MESSAGE-----
    Charset: utf-8
    Version: GnuPG v2
    Qqvms5xQ160
    ...
    -----END PGP MESSAGE-----

**Wie GPG4WIN auf dem PC installieren?**<br>
GPG4WIN ist eine Free-Windows-Variante von GnuPG bzw. OpenPGP. (PGP wäre übrigens die kommerzielle Variante.)
GPG4WIN beinhaltet den GnuPG-Zertifikatsmanager Kleopatra. Mit diesem kann man neue Schlüsselpaare erstellen und bestehende importieren und verwalten.
Im weiteren ist es in Kleopatra möglich, Nachrichten zu verschlüsseln und/oder zu signieren.
Bei diesem ersten Schritt muss noch nicht unbedingt ein Schlüsselpaar erzeugt werden, das geht später immer noch. Falls doch, empfiehlt es sich, einen Test-EMail-Account bereit zu halten. 
[Hier kann man GPG4WIN herunterladen.](https://www.gpg4win.de)

**Wie mit GPG4WIN/Kleopatra ein Schlüsselpaar erstellen?**

1. Starten des **gpg4win-Zertifikatsmanagers Kleopatra**.
2. Falls das bei der Installation von gpg4win noch nicht geschehen ist, erzeugen eines persönlichen Schlüsselpaars unter Datei/Neues OpenPGP-Schlüsselpaar...
Tipp: Beim Ausprobieren wird das Benutzen einer Test-E-Mail-Adresse empfohlen.
Hinweis: Alternativ wäre auch ein X.509-Schlüsselpaar denkbar. Dazu müsste man aber eine Beglaubigungsstelle einbeziehen, was den Rahmen dieser Übung sprengen würde.
3. Beim Erzeugen eines Schlüsselpaars wird eine sogenannte Passphrase verlangt. Dies ist ein Passwort, dass man später beim Erstellen und Öffnen einer verschlüsselten Nachricht eingeben muss. Diese Passphrase darf man darum keinesfalls vergessen und niemals weitergeben.
4. Exportieren des eigenen öffentlichen Schlüssels. Achtung: Hier den PublicKey und nicht den PrivateKey exportieren. Im Zweifelsfall mit einem Texteditor der Wahl das ASC-File überprüfen! In der ersten Zeile sollte BEGIN PGP PUBLIC KEY  BLOCK stehen.
5. PublicKey wie folgt umbenennen: Vorname_Nachname_PublicKey.asc
6. Kleopatra verwaltet die öffentlichen Schlüssel der Kommunikationspartner. Dazu muss man diese aber erst in Kleopatra einpflegen.
7. Testen: Um PGP auszuprobieren, soll man PublicKeys gegenseitig austauschen. Dies kann z.B. über das Internet geschehen. Im Schulbetrieb kann man ausnahmsweise vertrauen, dass der Schlüssel auch von der Personen stammt, auf die der Dateiname hinweist. Ausserhalb der Schule ist das selbstverständlich ein NoGo.

*(Tipp: Wenn sie in gpg4win/Kleopatra Schlüssel exportieren oder verschlüsselte Nachrichten erstellen, erhalten sie Dateien, die kryptisch benannt sind wie z.B. CD4C2D46582379E586CD29A26C423DAA26C49EFE.asc<br>
Die Endung asc ist ein Hinweis dafür, dass es sich um eine ASCII-Datei handelt, die mit einem Texteditor wie z.B. Notepad++ geöffnet werden kann. Damit lässt sich, wie sie nun wissen, zumindest feststellen, ob die untersuchte Datei ein PrivateKey, PublicKey oder eine verschlüsselte Nachricht ist. Welcher Person diese zuzuordnen ist, bleibt allerdings verborgen. Darum empfiehlt es sich, den Dateinamen entsprechend umzubenennen, wie z.B. FelixMusterPublicKey.asc.)*

## Auftrag zu PGP4WIN

1. **gpg4win installieren:** Installieren Sie auf Ihrem PC/Notebook gpg4win. 
[Hier kann man gpg4win herunterladen](https://www.gpg4win.de/) GPG4WIN kann bei Sicherheits- und anderen Bedenken auch auf einer virtuelle Windows-Maschine mit z.B. vmWare installiert werden.
2. **Mit Gpg4win/Kleopatra eigenes Schlüsselpaar erzeugen:** Starten sie nun den gpg4win-Zertifikatsmanager Kleopatra und erstellen Sie Ihr eigenes Schlüsselpaar. Tauschen Sie die Public-Keys untereinander aus und pflegen Sie diese in Ihren Zertifikatsmanager ein.
3. **Fremden Public-Key verifizieren:** Wie können sie die Authentizität des Ausstellerschlüssels überprüfen? Stammt dieser Public-Key auch wirklich von der Person, von der ich dies annehme?
4. **OpenPGP-Schlüssel:** Woraus besteht bzw. woran erkennt man diesen?
5. **X.509-Schlüsselpaar:** Nochmals zur Schlüsselerzeugung in Kleopatra (Datei/Neues Schlüsselpaar...). Ein persönliches OpenPGP Schlüsselpaar haben wir ja bereits erstellt. Da gibt es aber auch noch das persönliche X.509-Schlüsselpaar. Probieren sie das auch mal aus! Was sind die Unterschiede zwischenden den beiden Schlüsselvarianten und was hat das mit S/MIME zu tun?
6. **Mit Gpg4win/Kleopatra eine Nachricht verschlüsseln:** Nun soll eine beliebige Datei (Nachricht als Text, Bild etc.) für ihren Kommunikationspartner verschlüsselt werden. Dies kann direkt in Kleopatra erfolgen. Stellen sie das verschlüsselte File ihrem Kommunikationspartner zur Verfügung. (Per E-Mail, USB-Stick etc.) Wenn dieser es entschlüsseln kann, wurde die Aufgabe erfolgreich erledigt.
7. **Mit Gpg4win/Kleopatra eine Nachricht signieren:** Nun soll eine beliebige Datei (Text, Bild etc.) für ihren Kommunikationspartner signiert werden. Dies kann ebenfalls wieder direkt in Kleopatra erfolgen. Stellen sie das File inklusive Signatur ihrem Kommunikationspartner zur Verfügung. (Per E-Mail, USB-Stick etc.) Wenn dieser mit der Signatur die Echtheit ihres Files verifizieren kann, wurde die Aufgabe erfolgreich erledigt.
8. **Mit Gpg4win/Kleopatra eine Nachricht verschlüsseln und signieren:** In dieser Aufgabe soll eine beliebige Datei (Text, Bild etc.) für ihren Kommunikationspartner verschlüsselt und signiert werden. Wiederum in Kleopatra. Stellen sie das File inklusive Signatur ihrem Kommunikationspartner zur Verfügung. (Per E-Mail, USB-Stick etc.) Wenn dieser das File entschlüsseln und dank der Signatur den Absender verifizieren kann, wurde die Aufgabe erfolgreich erledigt.

