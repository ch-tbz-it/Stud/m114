# B.2 Verlustbehaftete Komprimierung
*Dauer: 4 Lektionen<br>
Autor: ARJ<br>
Letzte Änderung: 16.6.2023*

Verlustbehafteter Komprimierung begegnet man eigentlich nur bei Bild, Film und Ton. Man versucht mit geeigneten Verfahren in einer Datei den Informationsgehalt derart zu reduzieren, dass das subjektive Empfinden des Datei-Konsumenten die Datenreduktion möglichst nicht wahrnimmt. Dabei gibt es verschiedene Ansätze und oft auch Kombinationen davon.<br><br>

# B2.1. Bildauflösung, Bildwiederholungen oder Abtastung reduzieren
Die einfacheren Datenreduktionsvarianten sehen vor, die Auflösungen oder Bildwiederholrate zu reduzieren:

* **Bildgrösse:** Reduziert man bei einem Original-Bild von 1000 Pixel x 1000 Pixel Grösse sowohl Länge als auch Breite um die Hälfte, erhält man eine viermal kleinere Datei.

* **Farbauflösung:** Reduziert man zum Beispiel bei einem TrueColor-Bild die Farbauflösung von ursprünglich pro Grundfarben (Rot,Grün,Blau) 8 Bit bzw. 256 Abstufungen auf 4 Bit bzw. 16 Abstufungen), ist man zwar mit nur noch 4'096 unterschiedlichen Farbe weit entfernt von TrueColor mit 16'777'216 unterschiedlichen Farbe, hat die Dateigrösse damit aber halbiert.

* **Bildwiederholrate:** Beim Kino wird seit den Anfängen mit 24 Bilder pro Sekunde gerechnet. Aber schon ab 12 Bilder pro Sekunde würde eine Bewegung einigermassen flüssig zu erkenne sein. Ersparnis: 50%

* **Samplingrate reduzieren:** Wird ein analoges Signal wie z.B. das elektrische Signal eines Mikrophons digitalisiert, läuft das wie folgt ab: In regelmässigen Abständen wird die Amplitude des elektrischen Signals gemessen und in einen digitale Wert umgewandelt. Bei Audioaufnahmen sind übliche Werte 44.1kHz. Gemäss Abtasttheorem kann damit ein analoges Audiosignal bis 22kHz, was der theoretischen oberen Grenzfrequenz des menschlichen Gehörs entspricht, aufgelöst werden. Reduziert man nun die Samplingrate auf z.B. 8kHz, scherbelts zwar gehörig aus dem Lautsprecher, einem Gespräch kann man aber immer noch folgen, wie die historische Telefongrenzfrequenz von 4kHz beweist. Theoretische Ersparnis bei einer Samplingrate-Reduktion von 44.1kHz auf 8kHz: Datei wir ca. 5x kleiner. Man könne nun auch noch an der Auflösung der Amplitude herumschrauben und anstatt z.B. 16 Bit nur 8 Bit auflösen, was die Dateigrösse nochmals halbieren würde.<br><br>


# B2.2. Eine Farbtabelle benutzen
Anstatt jedes einzelne Pixel mit den drei Grundfarben RGB in 3x8Bit zu beschreiben, kann man die Farbinformation eines Pixels auch in einer Farbtabelle referenzieren. Das lohnt sich allerdings nur, wenn das reduzierte Farbangebot keine Rolle spielt. Mit Dithering kann man zwar etwas tricksen, indem man benachbarte Pixel mit unterschiedlichen Farbinformationen versieht, die dann bei der Betrachtung aus einer gewissen Distanz zu einer imaginären Drittfarbe verschmelzen.<br>
Bsp.: GIF mit 8 Bit Farbauflösung (ergibt 256 Farben).
![Farbtabelle](/GITressourcen/Daten_codieren/Farbtabelle.jpg)
<br><br><br>

# B2.3 Datenreduktion mit Subsampling (Unterabtastung)
Bevor man das Subsampling verstehen kann, muss man ein weiteres, im Multimediabereich wichtiges Farbmodell vorstellen:

## B2.3.1. Das Helligkeits-Farbigkeits-Modell oder Luminanz/Chrominanz-Modell:

![RGBYUV](/GITressourcen/Daten_codieren/RGBYUV.jpg)

Das **YCbCr-Helligkeits-Farbigkeits-Modell** (gemäss CCIR-601 bzw. IEC 601-Standard) wurde für das Digitalfernsehen entwickelt. Ausserdem wird es für digitale Bild- und Videoaufzeichnung, bei JPG-Bildern, MPEG-Videos und damit auch bei DVDs, sowie den meisten anderen digitalen Videoformaten verwendet. Es teilt die Farbinformation in die (Grund-)Helligkeit Y und die Farbigkeit, bestehend aus den zwei Farbkomponenten Cb (Blue-Yellow Chrominance) und Cr (Red-Green Chrominance) auf.<br><br>
Die Helligkeit entspricht der Hellempfindlichkeit des Auges, die im grünen Spektralbereich am grössten ist. Chrominance oder kurz Chroma bedeutet Buntheit.<br><br>
Die unterschiedliche Wahrnehmung von Y gegenüber den Cb- und Cr-Kanälen entspricht der Entwicklung der Farb- und Helligkeitsverteilung in der Natur: Im Laufe der Evolution hat sich der menschliche Sehsinn daran angepasst. Das Auge kann geringe Helligkeitsunterschiede besser erkennen als kleine Farbtonunterschiede, und diese wiederum besser als kleine Farbsättigungsunterschiede. So ist ein Text grau auf schwarz geschrieben gut zu lesen, blau auf rot geschrieben bei gleicher Grundhelligkeit jedoch nur sehr schlecht.<br><br>
Die Analogie zum menschlichen Sehsinn wird für einen grossen Vorteil von YCbCr genutzt: die Farbunterabtastung (engl. chroma subsampling). Dabei wird die Abtastrate und damit die Datenmenge der Chrominanz-Kanäle Cb und Cr gegenüber der Abtastrate des Luminanz-Kanals Y reduziert, ohne dass es zu einer spürbaren Qualitätsverringerung kommt. So kann man z. B. mit der JPEG-Komprimierung eine nicht unerhebliche Datenmenge einsparen.<br><br>
Das YUV-Farbmodell der analogen Fernsehtechnik wird manchmal fälschlicherweise mit YCbCr für digitale Darstellung von Farbvideosignalen gleichgesetzt. Der Einfachheithalber bedienen wir uns für die folgenden Erklärungen bei dem YCbCr-ähnlichen YUV-Farbmodell. Wer es ganz genau wissen will, konsultiert die einschlägige Fachliteratur oder informiert sich bei den entsprechenden Beiträgen auf Wikipedia.

## B2.3.2. Luminanzkanal: Die Umwandlung Farbbild zu Graustufenbild
![RGBtoSW](/GITressourcen/Daten_codieren/RGBtoSW.jpg)
In bestimmten Fällen ist es nötig, das Farbbild in ein Graustufenbild umzuwandeln. Zum Beispiel bei dem im nächsten Abschnitt behandelten Farbmodel YCbCr.<br>
Bei der Umwandlung werden die drei Farbanteile RGB verschieden gewichtet. Dies hat einen historischen Hintergrund: Der frühzeitliche Mensch als Jäger und Sammler war vor allem auf eine hohe Auflösung im Grünbereich (Wälder, Wiesen etc.) angewiesen, um Beute oder herannahende Gefahr besser erkennen zu können. Die Farbe Blau war da eher seltener und darum weniger wichtig. Da diese Aspekte des Farbensehens des menschlichen Auges berücksichtigt werden müssen. So wird beispielsweise Grün heller wahrgenommen als Rot, dieses wiederum heller als Blau. Diese unterschiedliche Gewichtung wird in folgender Umrechnungsformel berücksichtigt: Luminanz (Y) = 0.3 x Rot + 0.6 x Grün + 0.1 x Blau<br><br>
Gleich selber mal ausprobieren:<br>
[Colorizer-Tool Farbmixer inkl. YCbCr](https://colorizer.org/)

* RGB 255/255/255 ergibt in YCbCr....
* RGB 0/0/0 ergibt in YCbCr....
* Y:1, Cb:0, Cr:0 entspricht der Farbe ....
* Y:0, Cb:0, Cr:0 entspricht der Farbe ....
* Y:0, Cb:1, Cr:0 entspricht der Farbe ....
* Y:0, Cb:-1, Cr:0 entspricht der Farbe ....
* Y:0, Cb:0, Cr:1 entspricht der Farbe ....
* Y:0, Cb:0, Cr:-1 entspricht der Farbe ....
* Y:0.3, Cb:0.5, Cr:-0.17 entspricht der Farbe ....


## B2.3.3 Die Unterabtastung / Subsampling
Mit Subsampling meint man Farbunterabtastung. Auch hier wird die Bildauflösung reduziert. Allerdings nur partiell. Dazu ein Beispiel: Reduziert man ein HD1080-TV-Bild von 1920x1080 auf 960x540 hat man zwar die Dateigrösse geviertelt, möchte man das Bild aber wieder auf einem HD1080-Projektor anschauen, wirkt es unscharf, weil es von 960x540 wieder auf 1920x1080 aufgeblasen werden muss. Bei der Unterabtastung hat man einen anderen Weg gewählt: Man wandelt das RGB-Bild in ein YCbCr (Luminanz/ChrominanzBlau/ChrominanzRot) um. Anstatt wie beim vorangegangenen Beispiel alle drei Kanäle (RGB) in der Pixelauflösung zu reduzieren, wird dies bei Chroma-Subsampling nur in den beiden Chrominanz-Kanälen getan. Damit erält man zwar eine etwas ungenauere Einfärbung, die Bildschärfe (Luninanz) bleibt aber vollständig erhalten. Es sind folgende 
Chroma-Subsamplings vorgesehen: 4:4:4, 4:2:2, 4:1:1, 4:2:0. Und was wird damit eingespart? Bei 4:1:1 bedeutet dies Y=100%, Cb=25%, Cr=25% und somit gegenüber dem Original (300%) Dateigrösse halbiert.<br><br>


# B2.4. Weitere mathematische Verfahren
Wirklich schwierig wird's erst bei den mathematischen Verfahren. Hier brauchts schon einen Hochschulabschluss, um die Technik dahinter zu verstehen wie z.B. bei DCT (Discrete Cosine Transformation) für JPG-Komprimierung, wo in Kombination zwar einige bereits besprochene Verfahren wie Huffman, RLC, Chroma-Subsampling zur Anwendung kommen, aber eben auch die diskrete Kosinustransformation, wo uns die mathematischen Kenntnisse dazu fehlen. Wer's trotzdem wissen will, hier ein niveaugerechter Erklärungsversuch:

## B2.4.1 DCT - Die Verarbeitungsschritte

**1.** Als vorbereitender und erster Schritt wird das RGB-Bild in seinen Luminanzanteil und den beiden Crominanzanteilen umgewandelt. Danach wird die Auflösung in den beiden Chrominanzkanälen reduziert. (Chroma-Subsampling) 
Die drei Kanäle (1xLuminanz, 2xCrominanz) werden ab hier separat verarbeitet. Jeder  Kanal wird nun in 8x8 Pixelblöcke aufgeteilt.<br><br>
**2.** Auf jeden dieser 8x8 Pixelblöcke wird die diskrete Kosinustransformation angewendet. Vereinfacht ausgedrückt bedeutet dies, dass die 8x8 Bildwertematrix vom Bildbereich in den sogenannten Frequenzbereich transformiert wird. Im Bildbereich prägen Unterschiede in den Helligkeitswerten die Wertematrix, wohingegen im Frequenzbereich die Schnelligkeit der Helligkeitsänderungen entscheidend sind. (Etwas vereinfacht ausgedrückt: Scharfe Bilder ergeben schnelle Helligkeitsänderungen und damit viele hohe und unterschiedliche DCT-Werte, unscharfe langsamere und somit tiefere und weniger unterschiedliche DCT-Werte.)<br><br>

**3.** Die DCT-transformierten Werte werden nun quantisiert. Unter Quantisierung ist etwa dasselbe zu verstehen, wie das Notenrunden bei Schulprüfungen: Wenn die Schulnote mit einer Genauigkeit von 1/10 festgehalten wird, hat man mehr mögliche Notenwerte als wenn die Note auf 0.5 gerundet wird. Allerdings verliert man mit der gröberen, weil ungenaueren 0.5-er Notenskala auch an Aussagekraft. Dasselbe gilt für die quantisierten DCT-Werte. Bis jetzt hat man allerdings noch keine eigentliche Datenreduktion erreicht.<br><br>

**4.** Wie geht's weiter: Abhängig von der Stärke der Quantisierung der DCT-Werte (im Extremfall werden die meisten Werte zu 0) erhält die 8x8-Matrix eine geeignete Form, für eine anschliesend effiziente RL-Codierung (RLC) und zusätzlich VL-Codierung (VLC).<br><br>

**Schlussfolgerung:** Wenn bei der DCT zu stark quantisiert wird, gleichen sich die Farben der 8x8 Pixelblöcke einander an und es entsteht eine Blockstruktur. Man spricht dann von Blocking-Artefakten.
![JPGArtefakte.jpg](/GITressourcen/Daten_komprimieren/JPGArtefakte.jpg)
<br><br>

Die verschiedenen Verarbeitungsschritte werden auch in diesem Dokument (Printscreens aus der App DCTDemo) aufgezeigt: 
[DCTDemo.pdf](/GITressourcen/Daten_komprimieren/DCTDemo.pdf)<br>
Die direkte Kosinustransformation (DCT) wird in diesen beiden Videos genauer erklärt: 
[DCT-Video (Deutsch)](https://www.youtube.com/watch?v=7fhHQgu2OcY) 
mit begleitender Webseite [DCT-Text (Deutsch)](http://weitz.de/dct/) und eine englisch gesprochene, alternative Variante [DCT-Video (Englisch)](https://www.youtube.com/watch?v=Q2aEzeMDHMA)<br><br><br>

## B2.4.2 MP3 für Audio
MP3 ist eigentlich MPEG-1/2 Audio Layer 3 und damit ein Verfahren zur verlustbehafteten Kompression digital gespeicherter Audiodaten. Zur Komprimierung, die gegenüber einer Original-WAV-Datei durchaus bis zu über 85% betragen kann,
werden psychoakustische Effekte der menschlichen Wahrnehmung von Tönen und Geräuschen ausgenutz. Von MP3-Playern unterstützt werden Datenraten zwischen 8 kb/s und 320 kb/s. Wer mehr dazu erfahren will, dem sei z.B. der Wikipedia-Eintrag zu MP3 empfohlen.<br><br><br>


# B2.5 Bei Video nur Differenzbilder speichern (Interframe-Komprimierung)
Bisher haben wir ausschliesslich Verfahren betrachtet, die sich auf ein einzelnes Bild beziehen. Man bezeichnet dies auch **Intraframe-Komprimierung**. Es ist aber auch möglich innerhalb einer Bildsequenz Bandbreite oder Speicher zu sparen. Dies nennt man dann **Interframe-Komprimierung**. Und das geht so: Anstatt in einem Videostream pro Sekunde 24 komplette Bilder zu verschicken (oder speichern), könnte man ja nach einem Vollbild (i-Frame) nur noch die Änderung des Bildinhalts zum vorangegangenen Bild übermitteln. Allerdings darf man das nicht übertreiben. In regelmässigen Abständen sollte ein Vollbild verfügbar sein, damit die Filmwiedergabe bei einem Übermittlungsfehler neu synchronisieren kann. Man spricht dabei von einer **GOP** (Group-of-Pictures). Je nach Dynamik der Filmszene gibt das eine immense Einsparung an Daten. Filmt man 10 Minuten lang seine geschlossene Haustüre ohne weitere Action, würde dies bei GOP25 einer Datenreduktion von so etwa 96% bedeuten. BTW: Ein ähnliches Verfahrn kennen wir beim Backup, wo zwischen Fullbackup und inkrementellem Backup unterschieden wird.<br>
![Interframe.jpg](/GITressourcen/Daten_komprimieren/Interframe.jpg)
![GOPBsp.jpg](/GITressourcen/Daten_komprimieren/GOPBsp.jpg)
<br><br>


# B2.6. Speicherbelegung: Ein Vergleichsbeispiel
Im folgenden eine Übersicht über den Speicherbedarf für eine 1000 Pixel x 1000 Pixel Rastergarfik:
![Farbkreis](/GITressourcen/Daten_komprimieren/Farbkreis.jpg)

* Theoretische Speichergrösse: 1000 x 1000 = 1'000'000 Bildpunkte. Das bedeutet pro Pixel (3 Byte): 1'000'000 x 3 = 3'000'000 Byte.
* GIF-Datei: 71'903 Byte (Artefakte deutlich sichtbar, wegen Beschränkung auf 256 unterschiedliche Farben.  
Alleinstellungsmerkmal von GIF: Animated GIF)
* PNG: 356'476 Byte (keine sichtbaren Artefakte)
* TIF: 3'021'496 Byte (kein Informationsverlust. Das Archivformat schlechthin)
* TIF mit LZW-Komprimierung: 507'660 Byte (kein Informationsverlust)
* JPG mit tiefster Kompressionsrate: 271'201 Byte (keine sichtbaren Artefakte)
* JPG mit hoher Kompressionsrate:: 32'810 Byte (Blockartefakte deutlich sichtbar)

Diese sechs Bilddateien können als ZIP-Datei hier heruntergeladen werden: 
[Samples.zip](/GITressourcen/Daten_komprimieren/Samples.zip)<br><br>


# B2.7. Wissensfragen und Berechnungsaufgaben (1 Lektion)

1. Was ist der Unterschied zwischen dem Interlaced Mode und dem Progressive Mode?
2. Ein RGB-Farbbild benutzt nur die Farbe Weiss als Hintergrund und ein Hellblau mit folgenden Werten: R=33, G=121, B=239 (8 Bit pro Farbkanal). Das Bild soll in ein Graustufenbild umgewandelt werden. Berechnen sie den für das Hellblau entsprechende Grauwert. (8 Bit pro Farbkanal)
3. Was versteht man unter Artefakten und welche kennen sie?
4. Berechnen Sie die Bandbreite in GigaBit per Second oder kurz Gbps für die Übertragung eines unkomprimierten digitalen Videosignals HD1080i50 ohne Unterabtastung und 8 Bit Auflösung pro Kanal.
5. Nach wie vielen Minuten unkomprimierten HD1080i50 Video wäre eine DVD-5 (Single-Layer DVD mit 4.7GB) voll?
6. Berechnen sie, wieviel Speicher eingespart wird, wenn ein Bild mit Subsampling 4:1:1 komprimiert wird.
7. Was ist der Unterschied zwischen einem Codec und einem Mediencontainer?


# B2.8. Praxisaufgaben (1 bis 2 Lektionen)
Erstellen sie einen vertonten Videoclip von ca. 5..10 sec. Dauer. Dazu filmen sie z.B. mit ihrem Smartphone kleine Sequenzen und schneiden diese später zusammen. Man kann auch Audio-Material wie Begleitmusik etc. vom Internet herunterladen und einbauen. (Bei Veröffentlichung ihres Videos bitte die Copyrights beachten!) Verwenden sie die Videosoftware "Shotcut", um den Videoclip zu bearbeiten, allenfalls mit Text und Effekten zu ergänzen und schlussendlich zu rendern. Es sollen dabei verschiedene Zielmedien bedient werden, wie z.B. Youtube, TikTok, Facebook etc. Klären sie vorgängig ab, welche Formate (Container, Codecs) von diesen Videoplattformen unterstüzt werden. Sie sollen nach Abschluss dieses Kleinprojektes in der Lage sein, Begriffe wie Audiocodec, Videocodec und Mediencontainer zu verstehen und auseinanderzuhalten und eine Ahnung davon haben, welche Einstellungen zu welchen Ergebnissen (Datenreduktion, Artefakte etc.) führen.

* [Die zu bevorzugende Videoschnittsoftware: shotcut.org (Empfohlen wird die portable Version)](https://shotcut.org/download/)
* [Eine Alternative dazu ist: Free-Video-Editor openshot.org](https://www.openshot.org/de/)
* [Und ein reiner Medienkonverter wäre: Any Video Converter Free](https://www.any-video-converter.com/de/free-video-converter.html)

<br><br>
## Checkpoint
Welche Verfahren können bei Bild bzw. Bewegtbild (Movie) zu einer Einsparung von Datenspeicher bzw. moderateren Auslastung des Übertragungskanals beitragen?  
