# B.1 Verlustlose Komprimierung
*Dauer: 4 Lektionen<br>
Autor: ARJ<br>
Letzte Änderung: 21.8.2022*


## VLC (Variable Length Coding)
Bei VLC wird einer festen Anzahl an Quellsymbolen jeweils Codewörter mit variabler Länge zugeordnet. Das im Gegensatz zu z.B. einem ASCII-Code, wo einem Quellsymbol jeweils ein Codewort mit fixer Länge von 8 Bit zugeordnet wird.

* **Der Morsecode**<br>
Der Morsecode, erfunden um 1838 von Samuel Morse und Alfred Lewis Vail, ist ein Verfahren, um in Seefunk und Telegrafie elektrisch, optisch oder akustisch verlustlos komprimiert Buchstaben, Zahlen und weitere Zeichen zu übermitteln. Der Morsecode kann man als Codetabelle oder als binären Baum darstellen.
![Morsecode](/GITressourcen/Daten_komprimieren/Morsecode.jpg)
Hier können sie gleich mal üben: ![Morsecode.wav](/GITressourcen/Daten_komprimieren/Morsecode.wav)<br>
Betrachtet man das folgende Beispiel, zeigt sich ein nicht unwesentlicher Nachteil dieses Verfahrens:<br>
Analysieren sie die folgende Codesequenz: ..-....-<br>
Die könnte bedeuten: IDEA I=.. D=-.. E=. A=.-<br>
oder aber USA? U=..- S=... A=.-<br>
vielleicht aber auch UHT (UltraHighTemperatur)? U=..- H=.... T=-<br>
schlussendlich ist es doch ein Fussballverein FV? F=..-. V=...-<br>
Also keinesfalls eindeutig! Der Nachteil beim Morsecode liegt darin, dass es ohne spezielles Trennzeichen oder Delimiter (Beim Morsecode eine kurze Pause) oft Missverständnisse geben kann, wo das Zeichen beginnt und wo es endet. Dieses Problem besteht z.B. bei der nachfolgenden Huffman-Kodierung nicht, weil hier die Eigenschaft erfüllt sein muss, dass kein Codewort der Beginn eines anderen Codewortes sein darf.

* **Der Huffmancode**<br>
Um einen Huffman-Code für eine feste Anzahl von Quellsymbolen zu entwickeln, baut man Schritt für Schritt einen binären Baum auf, ähnlich dem beim Morsecode. Wie das geschieht, soll das folgende Beispiel zeigen, wo als Quellsymbole nur die Buchstaben für das Wort ERDBEERE Verwendung finden:
![HuffmanA](/GITressourcen/Daten_komprimieren/HuffmanA.jpg)
Wir stellen fest, dass das in ERDBEERE am häufigsten auftretende Quellsymbol E das kürzeste Codwort erhalten hat. Mit den für die Speicherung benötigten 14 Bit ist man wesentlich effizienter, als wenn man ERDBEERE mit ASCII-Codierung gespeichert hätte, was einem Speicherbedarf von 8x8Bit=64Bit entsprochen hätte. Die Versuchung liegt nahe, das Alphabet gemäss Häufigkeitsverteilung (der Buchstabe E kommt in deutschen und englischen Texten am häufigsten vor) in Huffman zu codieren und ASCII in Rente zu schicken. Was meinen sie dazu?<br>
Bei diesem Verfahren ist die Quellsymbolart übrigens nicht nur auf Buchstaben beschränkt.<br><br>
In diesem Beipiel wurde das Wort ERDBEERE mit einem N zu ERDBEEREN ergänzt. Mehrere Lösungen sind nun möglich. Allerdings führen alle zur selben Codeeffizienz. (Das letzte Beispiel zeigt übrigens einen falschen Aufbau des binären Baums, was auch nicht zur selben Codeeffizienz wie bei den korrekten Beispielen führt)<br>
Da bei diesem Beispiel mehrere Codevarianten möglich sind, muss für die Huffman-Decodierung die gewählte Codetabelle mitgeliefert werden. Dies hat allerdings, im Vergleich zur ASCII-Variante, einen negativ Einfluss auf die Speicherbilanz. Der **Verwaltungsoverhead** ist im Vergleich zu den **Nutzdaten** zu hoch. Die würde sich ändern, wenn der zu komprimierende Text markant länger wäre.
![HuffmanB](/GITressourcen/Daten_komprimieren/HuffmanB.jpg)

## RLC (Run Length Coding) bzw. RLE (Run Length Encoding)
Mit Run Length Coding (bzw. RLE was Run Length Encoding bedeutet) ist eine Lauflängenkodierung gemeint. Jede Sequenz von identischen Symbolen soll durch deren Anzahl und ggf. das Symbol ersetzt werden. Somit werden nur die Stellen markiert, an denen sich das Symbol in der Nachricht ändert. Effizient bei langen Wiederholungen.
![RLC1](/GITressourcen/Daten_komprimieren/RLC1.jpg)
RLE-Bit-Berechnung für die ersten vier Zeilen:
![RLC2](/GITressourcen/Daten_komprimieren/RLC2.jpg)
Der Speicherbedarf für das RLC-komprimierte Bild:<br>
Ab erstem Pixel oben links: 31 x Weiss, 2 x Schwarz, 11 x Weiss, 3 x Schwarz, 2 x Weiss, 6 x Schwarz, 6 x Weiss, 6 x Schwarz, 1 x Weiss, 8 x Schwarz, 4 x Weiss<br>
Total: 11 Zahlen oder Farbwechsel. Grösste Zahl: 31 (Benötigt 5 Bit)<br>
Die Zahlen im Binärcode: 11111 - 00010 - 01011 - 00011 - 00010 - 00110 - 00110 - 00001 - 01000 - 00100<br>
Zusammenfassend **RLC:** 11 x 5Bit = **55Bit** (Der Abstand mit Bindestrich wurde nur zwecks besserer Lesbarkeit hinzugefügt)<br>
Der Speicherbedarf für das normale **Bitmap:** 4 Zeilen zu je 20 Pixel = **80Bit**

## Lexikalisches Verfahren LZW (Lempel-Ziv-Welch-Algorithmus)
Ein anderer Ansatz verfolgt der Lempel-Ziv-Welch-Algorithmus: Die Idee dahinter ist, wiederkehrende binäre Muster nicht erneut zu speichern, sondern eine Referenz darauf. Es wird eine Art Wörterbuch erstellt. LZW wird häufig bei der Datenreduktion von Grafikformaten (GIF, TIFF) verwendet.<br><br>
**LZW-Transformation am Textbeispiel ENTGEGENGENOMMEN erklärt**<br>
Das Wort «ENTGEGENGENOMMEN» soll verlustlos datenreduziert bzw. komprimiert werden. Die Werte 0 bis 255 sind den ASCII-Zeichen vorbehalten. Die Werte ab 256 sind Indexe, die auf einen Wörterbucheintrag verweisen:
![LZW-Transformation](/GITressourcen/Daten_komprimieren/LZW1.jpg)
<br><br>
**LZW-Rücktransformation**<br>
![LZW-Rücktransformation](/GITressourcen/Daten_komprimieren/LZW2.jpg)

## Aufgaben zu Komprimierung

1. **Huffman-Algorithmus:** (Teamarbeit). Jeder denkt für sich ein Wort mit ca. 15 Buchstaben aus und erstellt dazu die Huffman-Codetabelle und das entsprechend komprimierte Wort in HEX-Darstellung. Nun werden die Codes inklusive der Codetabelle gegenseitig ausgetauscht. Kann ihr Partner ihr gewähltes Wort richtig dekomprimieren?
2. **RLC/E-Verfahren:** Sie erhalten diesen RL-Code:<br>
010100011110010010010010010010010010010110010110010010010010010010010010001<br>
Folgendes ist ihnen dazu bekannt: Es handelt sich um eine quadratische Schwarz-Weiss-Rastergrafik mit einer Kantenlänge von 8 Pixel. Es wird links oben mit der Farbe Weiss begonnen. Eine Farbe kann sich nicht mehr als siebenmal wiederholen. Zeichnen sie die Grafik auf. Was stellt sie dar? 
3. **LZW-Verfahren:** Erstellen sie die LZW-Codierung für das Wort «ANANAS» und überprüfen sie mit der Dekodierung ihr Resultat. Danach versuchen sie den erhaltenen LZW-Code «ERDBE<256>KL<260>» zu dekomprimieren.
4. **ZIP-Komprimierung:** Wir wollen die Effizienz bei der ZIP-Komprimierung untersuchen. Dazu sollen sie ASCII-Textdateien erstellen. 
Die erste enthält 10, die zweite 100, die dritte 1000, die vierte 10'000 und die fünfte 100'000 ASCII-Zeichen. Achten sie darauf, dass die Zeichen möglichst zufällig gewählt werden. Auf dem Internet findet man entsprechende Textgeneratoren. Kopieren sie jede dieser fünf Textdateien in eine eigene ZIP-Datei. In der Folge erhalten sie fünf ZIP-Dateien. Werten sie nun in einer EXCEL-Tabelle die erforderlichen Speichergrössen aus: ASCII-Datei-Grösse zu ZIP-Datei-Grösse. Versuchen sie nun, ihr Resultat zu interpretieren bzw. zu begründen. Tipp: Sie können in EXCEL Zahlenreihen auch grafisch anzeigen.<br>
Nun legen wir noch einen drauf: Erstellen sie eine ASCII-Textdatei mit 100'000 Zeichen. Diemal aber nicht random-befüllt, sondern ausschliesslich mit dem Buchstaben A, danach zippen sie. Vergleichen sie nun die beiden ZIP-Dateien. Wie erklären sie sich den Unterschied der Speichergrössen?<br>
Zu guter letzt wollen wir untersuchen, was die ZIP-Komprimierung bringt, wenn die Originaldatei, wie beim JPG-Bildformat, bereits komprimiert (DCT) vorliegt. Dazu erhalten sie die zwei folgenden Bilder:
[ZIPTestHi.jpg](/GITressourcen/Daten_komprimieren/ZIPTestHi.jpg) und 
[ZIPTestLo.jpg](/GITressourcen/Daten_komprimieren/ZIPTestLo.jpg)<br>
Gehen sie nun gleich vor, wie beim vorangegangenen Untersuch der Textdateien. Begründen sie ihr Resultat.
5. **BWT (Burrows-Wheeler-Transformation):** Bei diesem Verfahren wird das Original mit ein paar Tricks so umgestellt, dass danach RLC/E seine volle Wirkung entfalten kann. Suchen sie im Internet nach Informationen zu diesem Verfahren. Lösen sie nun die folgenden beiden Aufgaben:<br>
Erstellen sie die BWT-Transformation für das Wort ANANAS und überprüfen sie mit der rücktransformation ihr Resultat.<br>
Sie erhalten den Code IICRTGH6 in der Burrows-Wheeler-Transformation. Welches Wort verbirgt sich dahinter?




