# B. Daten komprimieren
Für dieses Kapitel stehen an der Schule insgesamt 8 Lektionen zu je 45 Minuten zur Verfügung.

![Komprimieren](/GITressourcen/Daten_komprimieren/Komprimieren.jpg)
Beim Thema Komprimieren wird wie folgt unterschieden:

* **Verlustlose Komprimierung:** Es gibt viele verschiedene theoretische Konzepte und Algorithmen für verlustlose Komprimierung wie z.B. **VLC (Variable Length Coding)** mit z.B. dem Morsecode oder Huffman, **RLC (Run Length Coding)**, **lexikalische Verfahren** wie z.B. LZ77/LZ78(Lempel-Ziv), LZW (Lempel-Ziv-Welch) oder Deflate, **Transformationsverfahren**, die selber allerdings keine Datenkompression durchführen wie z.B. BWT (Burrows-Wheeler-Transformation) und viele weitere. Allen gemeinsam ist, dass  komprimierte Daten verlustlos dekomprimiert werden können. Bsp.: Text, Applikationen, Datenbanken etc.<br>
Dauer: 4 Lektionen<br>
[Hier geht's zum Unterrichtsmaterial](/B. Daten komprimieren/B.1 Verlustlose Komprimierung)
* **Verlustbehaftete Komprimierung:** Bei den wirklichen Schwergewichten wie sie im Bereich Multimedia (Bild, Film, Ton) anzutreffen sind, genügen verlustlose Komprimierungsverfahren alleine nicht. Man versucht mit geeigneten Verfahren, in einer Datei den Informationsgehalt derart zu reduzieren, dass das subjektive Empfinden des Datei-Konsumenten die Datenreduktion nicht oder kaum wahrnimmt. Unter anderem gelingt dies mit Transformationsverfahren, die zwar selber noch keine Datenreduktion herbeiführen, einem darauffolgenden Komprimierungsverfahren wie z.B. Huffman, RLC, LZ78 etc. aber entsprechendes Potential bieten. Bsp.: DCT (Discrete Cosine Transformation) das unter anderem bei JPG zur Anwendung kommt. Verlustbehaftet komprimierte Daten entsprechen nach der Dekomprimierung nicht mehr dem Original.<br>
Dauer: 4 Lektionen<br>
[Hier geht's zum Unterrichtsmaterial](/B. Daten komprimieren/B.2 Verlustbehaftete Komprimierung)
