![M114Startbild](/GITressourcen/M114Startbild.jpg)

# Übersicht M114
**Codierungs-, Kompressions- und Verschlüsselungsverfahren einsetzen**<br>
Für dieses Modul sind an der TBZ-IT insgesamt 10 Schultage zu je 4 Lektionen vorgesehen. (Total 40L) 
Das hier bereitgestellte Schulmaterial sollte in  32 Lektionen bewältigt werden können. Weitere 8 Lektionen dienen der Reserve.  (Prüfungen, Lektionsausfälle, Feiertage, Sporttage etc.)<br>
Autor und Modulverantwortung für die TBZ-IT: [juerg.arnold@tbz.ch](juerg.arnold@tbz.ch) (ARJ)<br><br>
Alternative Unterlagen vom Modulverantwortlichen (ARJ) findet man im [Sharepoint](https://tbzedu.sharepoint.com/:f:/r/sites/campus/students/it/_read-only/M114-ARJ?csf=1&web=1&e=fPosZm)<br><br>

## A. Daten codieren (12L)
**Codierungen von Daten situationsbezogen auswählen und einsetzen. Aufzeigen, welche Auswirkung die Codierung auf die Darstellung von Daten hat.** Binäres- und hexadezimales Zahlensystem. Numerische (signed/unsigned) und alphanumerische (ASCII/Unicode) Codes. Zusammengesetzte Codierung. Barcodes. Farbcodierung in Bildern. Wahl eines geeigneten Bildformats.

* [Hier geht's zum Unterrichtsmaterial](/A. Daten codieren)
* [Ergänzendes Unterrichtsmaterial von ARJ zu Codes](https://www.juergarnold.ch/codesysteme.html)
* [Ergänzendes Unterrichtsmaterial von ARJ zu Multimedia](https://www.juergarnold.ch/videotechnik.html)

## B. Daten komprimieren (8L)
**Kompressionsverfahren gemäss Vorgaben für die Aufbewahrung, Wiederherstellung und Übertragung von Daten auswählen und einsetzen.** Verlustlose und verlustbehaftete Komprimierung. VLC (Huffman, RLC, BWT, LZW, DCT). Artefakte.

* [Hier geht's zum Unterrichtsmaterial](/B. Daten komprimieren)
* [Ergänzendes Unterrichtsmaterial von ARJ zu Kompression](https://www.juergarnold.ch/kompression.html)

## C. Grundlagen Verschlüsselungsverfahren (Kryptografie) (6L)
**Verschlüsselungsverfahren zur Sicherung von Daten gemäss Vorgaben gegen unbefugten Zugriff auf Datenspeicher auswählen und einsetzen.** Gesicherte Übertragungsverfahren für Dateien mit symmetrischen und asymmetrischen Verschlüsselungsverfahren nutzen. Dabei Aspekte wie Public/Private Key, Protokolle und Standards berücksichtigen. Verschiedene Verschlüsselungstechnologien hinsichtlich Aktualität, Verbreitung und Sicherheit bewerten. Schwachstellen erkennen und Vorschläge für alternative Technologien machen.

* [Hier geht's zum Unterrichtsmaterial](/C. Grundlagen Kryptografie)
* [Ergänzendes Unterrichtsmaterial von ARJ zu Kryptografie](https://www.juergarnold.ch/kryptografie.html)

## D. Gesicherte Datenübertragung (6L)
**Verschlüsselungsverfahren zur Sicherung von Daten gemäss Vorgaben gegen unbefugten Zugriff auf Übertragungswegen auswählen und einsetzen.** Signierte und verschlüsselte EMails. HTTPS, Zertifikate und Zertifizierungsstellen.

* [Hier geht's zum Unterrichtsmaterial](/D. Gesicherte Datenübertragung)
* [Ergänzendes Unterrichtsmaterial von ARJ zu Kryptografie](https://www.juergarnold.ch/kryptografie.html)

